## Usage


## Requirements

## Installation

JMBluetooth is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "Fairy"
```

## Author

JARMourato, joao.armourato@gmail.com

## License

JMBluetooth is available under the MIT license. See the LICENSE file for more info.
