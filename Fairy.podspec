Pod::Spec.new do |s|
  s.name = "Fairy"
  s.version = "0.1.1"
  s.summary = "The Bluetooth Fairy"
  s.license = 'MIT'
  s.homepage = "https://github.com/JARMourato/TBL"
  s.author           = { "JARMourato" => "joao.armourato@gmail.com" }
  s.source           = { :git => "https://JARMourato@bitbucket.org/JARMourato/fairy-dev.git" , :tag => s.version.to_s } 

  s.ios.deployment_target = '8.0'
  s.osx.deployment_target = '10.9'
  s.requires_arc = true

  s.source_files = 'Source/**/*.swift'

  s.requires_arc = true
  s.exclude_files = s.name.to_s + '.podspec','LICENSE','README.md'

  s.frameworks = 'CoreBluetooth'
end
