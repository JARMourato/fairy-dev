//
//  BluetoothLocalCharacteristicTests.swift
//  Fairy
//
//  Created by iOS on 16/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import XCTest
import CoreBluetooth
@testable import Fairy

class BluetoothLocalCharacteristicTests: XCTestCase {

    var uuidstring : String!
    var properties : [BluetoothCharacteristicProperties]!
    var value : NSData?
    var permissions : [BluetoothCharacteristicValueAttributePermissions]!
    var localCharacteristic : BluetoothLocalCharacteristic!
    
    override func setUp() {
        super.setUp()
        
        uuidstring = "CF4E122F-6DFA-4810-BE83-76E9B0D0A479"
        properties = []
        permissions = []
        value = nil
        localCharacteristic = BluetoothLocalCharacteristic(uuidString : uuidstring, properties : properties, staticValue : value, permissions :permissions)
    }
    
    func testLocalCharacteristicInitialization() {
        
        let cbcharacteristic = localCharacteristic.CoreBluetoothCharacteristic
        
        XCTAssert(localCharacteristic.UUIDString == uuidstring, "Local characteristic UUIDString should be equal to uuidstring")
        XCTAssert(localCharacteristic.value == value, "Local characteristic value should be equal to value")
        XCTAssert(localCharacteristic.subscribedCentrals.count == 0, "Local characteristic subscribed centrals should be zero")
        XCTAssert(localCharacteristic.centralsNeedingUpdate?.count == 0, "Local characteristic centrals needing update should be zero")
        
        XCTAssertEqual(localCharacteristic.description, cbcharacteristic.description,"local characteristic descripition should be equal to core bluetooth description")
        XCTAssertEqual(localCharacteristic.debugDescription, cbcharacteristic.debugDescription,"local characteristic debug descripition should be equal to core bluetooth debug description")
        XCTAssert(cbcharacteristic.subscribedCentrals?.count == 0, "Initially the core bluetooth subscribed centrals should be zero")
        
        //TODO: Test properties equality
        //TODO: Test permissions equality
    }
    
    func testUpdateCharacteristicSubscribedCentrals() {
        
        localCharacteristic.updateCharacteristic(subscribedCentrals: nil)
        
        XCTAssert(localCharacteristic.subscribedCentrals.count == 0, "Local characteristic subscribed centrals should be equal to zero")
        
        let c1 = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "B170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        let c2 = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "D170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        
        localCharacteristic.updateCharacteristic(subscribedCentrals: [c1,c2])
        
        XCTAssert(localCharacteristic.subscribedCentrals.count == 2, "Local characteristic subscribed centrals should be equal to two")
    }
    
    func testAddSubscribedCentralLocalCharacteristic() {
        let c1 = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "B170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        let crepeat = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "B170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        
        let added = localCharacteristic.addSubscribedCentral(c1)
        let notAdded = localCharacteristic.addSubscribedCentral(crepeat)
        
        XCTAssert(added, "Remote central c1 should be added")
        XCTAssert(!notAdded, "Remote central crepeat should not be added")
    }
    
    func testIsSubscribedCentralLocalCharacteristic() {
        let c1 = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "B170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        let c2 = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "D170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        
        localCharacteristic.addSubscribedCentral(c1)
        
        let isSubscribed = localCharacteristic.isSubscribedCentral(c1)
        let isNotSubscribed = localCharacteristic.isSubscribedCentral(c2)
        
        XCTAssert(isSubscribed, "Remote central c1 should be a subscribed central")
        XCTAssert(!isNotSubscribed, "Remote central c2 should not be subscribed central")
    }
    
    func testRemoveSubscribedCentralLocalCharacteristic() {
        let c1 = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "B170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        let crepeat = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "B170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        let c2 = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "D170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        
        localCharacteristic.addSubscribedCentral(c1)
        
        let removed = localCharacteristic.removeSubscribedCentral(c1)
        let removeRepeated = localCharacteristic.removeSubscribedCentral(crepeat)
        let notRemoved = localCharacteristic.removeSubscribedCentral(c2)
        
        XCTAssert(removed, "Remote central c1 should be removed")
        XCTAssert(!removeRepeated, "Remote central crepeated should not be removed")
        XCTAssert(!notRemoved, "Remote central c2 inexistent should not be removed")

    }
    
    func testAddNotUpdatedCentralLocalCharacteristic() {
        let c1 = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "B170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        let crepeat = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "B170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        
        let added = localCharacteristic.addNotUpdatedCentral(c1)
        let notAdded = localCharacteristic.addNotUpdatedCentral(crepeat)
        
        XCTAssert(added, "Remote central c1 should be added")
        XCTAssert(!notAdded, "Remote central crepeat should not be added")
    }
    
    func testRemoveUpdatedCentralLocalCharacteristic() {
        let c1 = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "B170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        let crepeat = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "B170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        let c2 = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "D170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        
        localCharacteristic.addNotUpdatedCentral(c1)
        
        let removed = localCharacteristic.removeUpdatedCentral(c1)
        let removeRepeated = localCharacteristic.removeUpdatedCentral(crepeat)
        let notRemoved = localCharacteristic.removeUpdatedCentral(c2)
        
        XCTAssert(removed, "Remote central c1 should be removed")
        XCTAssert(!removeRepeated, "Remote central crepeated should not be removed")
        XCTAssert(!notRemoved, "Remote central c2 inexistent should not be removed")

    }
    
    func testLocalCharacteristicEquality() {
        let equalCharacteristic = BluetoothLocalCharacteristic(uuidString : uuidstring, properties : properties, staticValue : value, permissions :permissions)
        let differentCharacteristic = BluetoothLocalCharacteristic(uuidString : "C7292120-5F58-4D9F-9907-E399C7DFD3D7", properties : properties, staticValue : value, permissions :permissions)
        
        XCTAssert(equalCharacteristic == localCharacteristic, "Should be equal characteristics")
        XCTAssert(!(differentCharacteristic == localCharacteristic), "Should be different characteristics")
    }
}
