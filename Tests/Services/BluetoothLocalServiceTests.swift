//
//  BluetoothLocalService.swift
//  Fairy
//
//  Created by iOS on 16/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import XCTest
import CoreBluetooth
@testable import Fairy

class BluetoothLocalServiceTests: XCTestCase {

    var uuidstring : String!
    var primary : Bool!
    
    var localService : BluetoothLocalService!
    
    override func setUp() {
        super.setUp()
        
        uuidstring = "CF4E122F-6DFA-4810-BE83-76E9B0D0A479"
        primary = true
        
        localService = BluetoothLocalService(uuidString: uuidstring, primaryService: primary)
    }
   
    func testLocalServiceInitialization(){

        let cbservice : CBService = localService.CoreBluetoothService
        
        XCTAssertEqual(localService.UUIDString, "CF4E122F-6DFA-4810-BE83-76E9B0D0A479","local service uuid should be equal to uuid")
        XCTAssertEqual(localService.primary, primary, "local service primary should be equal to primary")
        XCTAssert(localService.characteristics.count == 0, "Initially the local service characteristics should be zero")
        
        XCTAssertEqual(localService.description, cbservice.description,"local service descripition should be equal to core bluetooth description")
        XCTAssertEqual(localService.debugDescription, cbservice.debugDescription,"local service debug descripition should be equal to core bluetooth debug description")
        XCTAssert(cbservice.characteristics?.count == 0, "Initially the core bluetooth characteristics should be zero")
        
    }
    
    func testAddCharacteristicLocalService() {
        let localCharacteristicOne = BluetoothLocalCharacteristic(uuidString: "B170A2CF-8042-4A36-B104-9D8AA75B5095", properties: [], staticValue: nil, permissions: [])
        let localCharacteristicRepeated = BluetoothLocalCharacteristic(uuidString: "B170A2CF-8042-4A36-B104-9D8AA75B5095", properties: [], staticValue: nil, permissions: [])
        
        let addedOne = localService.addCharacteristic(localCharacteristicOne)
        let addedRepeated = localService.addCharacteristic(localCharacteristicRepeated)
        
        XCTAssert(addedOne, "Should be able to add characteristic one")
        XCTAssert(!addedRepeated, "Should not be able to add characteristic repeated")
    }
    
    func testHasCharacteristicLocalService() {
        let localCharacteristicOne = BluetoothLocalCharacteristic(uuidString: "B170A2CF-8042-4A36-B104-9D8AA75B5095", properties: [], staticValue: nil, permissions: [])
        localService.addCharacteristic(localCharacteristicOne)
        
        let hasCharateristicOneByString = localService.hasCharacteristic(localCharacteristicOne.UUIDString)
        let hasCharateristicOneByCBUUID = localService.hasCharacteristic(localCharacteristicOne.UUID)
        
        XCTAssert(hasCharateristicOneByString != nil, "Should be able to find characteristic one by string")
        XCTAssert(hasCharateristicOneByCBUUID != nil, "Should be able to find characteristic one by CBUUID")
        XCTAssert(hasCharateristicOneByString! == localCharacteristicOne, "Found characteristic by string should be equal to localCharacteristicOne")
        XCTAssert(hasCharateristicOneByCBUUID! == localCharacteristicOne, "Found characteristic by CBUUID should be equal to localCharacteristicOne")
        
        let notToBeFoundCharacteristic = BluetoothLocalCharacteristic(uuidString: "10281051-DE4B-489B-B4DF-2E740C4C59EE", properties: [], staticValue: nil, permissions: [])
        
        let notFoundCharateristicOneByString = localService.hasCharacteristic(notToBeFoundCharacteristic.UUIDString)
        let notFoundCharateristicOneByCBUUID = localService.hasCharacteristic(notToBeFoundCharacteristic.UUID)
        
        XCTAssert(notFoundCharateristicOneByString == nil, "Should not be able to find characteristic notToBeFoundCharacteristic by string")
        XCTAssert(notFoundCharateristicOneByCBUUID == nil, "Should not be able to find characteristic notToBeFoundCharacteristic by CBUUID")
    }
    
    func testRemoveCharacteristicLocalService() {
        let localCharacteristicOne = BluetoothLocalCharacteristic(uuidString: "B170A2CF-8042-4A36-B104-9D8AA75B5095", properties: [], staticValue: nil, permissions: [])
        localService.addCharacteristic(localCharacteristicOne)
        
        let removed = localService.removeCharacteristic(localCharacteristicOne)
        let notRemoved = localService.removeCharacteristic(localCharacteristicOne)
        
        XCTAssert(removed, "Should be able to remove characteristic one")
        XCTAssert(!notRemoved, "Should not be able to remove characteristic repeated")
    }
    
    func testAddSubscribedCentralLocalService() {
        let localCharacteristicOne = BluetoothLocalCharacteristic(uuidString: "B170A2CF-8042-4A36-B104-9D8AA75B5095", properties: [], staticValue: nil, permissions: [])
        let localCharacteristicTwo = BluetoothLocalCharacteristic(uuidString: "C7292120-5F58-4D9F-9907-E399C7DFD3D7", properties: [], staticValue: nil, permissions: [])
        localService.addCharacteristic(localCharacteristicOne)
        
        let central = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "B170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        
        let added = localService.addSubscribedCentral(central, forCharacteristic: localCharacteristicOne)
        let repeatedNotAdded = localService.addSubscribedCentral(central, forCharacteristic: localCharacteristicOne)
        let notAdded =  localService.addSubscribedCentral(central, forCharacteristic: localCharacteristicTwo)
        XCTAssert(added, "Should be able to add subscribed central to characteristic one")
        XCTAssert(!repeatedNotAdded, "Should not be able to add repeated subscribed central to characteristic one")
        XCTAssert(!notAdded, "Should not be able to add subscribed central to characteristic one")
    }
    
    func testRemoveSubscribedCentralLocalService() {
        let localCharacteristicOne = BluetoothLocalCharacteristic(uuidString: "B170A2CF-8042-4A36-B104-9D8AA75B5095", properties: [], staticValue: nil, permissions: [])
        let localCharacteristicTwo = BluetoothLocalCharacteristic(uuidString: "C7292120-5F58-4D9F-9907-E399C7DFD3D7", properties: [], staticValue: nil, permissions: [])
        localService.addCharacteristic(localCharacteristicOne)
        
        let central = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "B170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        localService.addSubscribedCentral(central, forCharacteristic: localCharacteristicOne)
        let removed = localService.removeSubscribedCentral(central, forCharacteristic: localCharacteristicOne)
        let notRemoved =  localService.removeSubscribedCentral(central, forCharacteristic: localCharacteristicOne)
        let notRemovedNonExisting =  localService.removeSubscribedCentral(central, forCharacteristic: localCharacteristicTwo)
        
        XCTAssert(removed, "Should be able to remove subscribed central from characteristic one")
        XCTAssert(!notRemoved, "Should not be able to remove subscribed central from characteristic one")
        XCTAssert(!notRemovedNonExisting, "Should not be able to remove subscribed central from characteristic one")
    }
    
    func testAddNotUpdatedCentralLocalService() {
        let localCharacteristicOne = BluetoothLocalCharacteristic(uuidString: "B170A2CF-8042-4A36-B104-9D8AA75B5095", properties: [], staticValue: nil, permissions: [])
        let localCharacteristicTwo = BluetoothLocalCharacteristic(uuidString: "C7292120-5F58-4D9F-9907-E399C7DFD3D7", properties: [], staticValue: nil, permissions: [])
        localService.addCharacteristic(localCharacteristicOne)
        
        let central = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "B170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        
        let added = localService.addNotUpdatedSubscribedCentral(central, forCharacteristic: localCharacteristicOne)
        let repeatedNotAdded = localService.addNotUpdatedSubscribedCentral(central, forCharacteristic: localCharacteristicOne)
        let notAdded =  localService.addNotUpdatedSubscribedCentral(central, forCharacteristic: localCharacteristicTwo)
        XCTAssert(added, "Should be able to add not updated central to characteristic one")
        XCTAssert(!repeatedNotAdded, "Should not be able to add repeated not updated central to characteristic one")
        XCTAssert(!notAdded, "Should not be able to add not updated central to characteristic one")
    }
    
    func testRemoveUpdatedCentralLocalService() {
        let localCharacteristicOne = BluetoothLocalCharacteristic(uuidString: "B170A2CF-8042-4A36-B104-9D8AA75B5095", properties: [], staticValue: nil, permissions: [])
        localService.addCharacteristic(localCharacteristicOne)
        let central = BluetoothRemoteCentral(mockUpIdentifier: NSUUID(UUIDString: "B170A2CF-8042-4A36-B104-9D8AA75B5095")!)
        localService.addNotUpdatedSubscribedCentral(central, forCharacteristic: localCharacteristicOne)
        
        let localCharacteristicTwo = BluetoothLocalCharacteristic(uuidString: "C7292120-5F58-4D9F-9907-E399C7DFD3D7", properties: [], staticValue: nil, permissions: [])
        
        let removed = localService.removeUpdatedCentral(central, forCharacteristic: localCharacteristicOne)
        let notRemoved =  localService.removeUpdatedCentral(central, forCharacteristic: localCharacteristicOne)
        let notRemovedNonExisting =  localService.removeUpdatedCentral(central, forCharacteristic: localCharacteristicTwo)
        XCTAssert(removed, "Should be able to remove not updated central from characteristic one")
        XCTAssert(!notRemoved, "Should not be able to remove not updated central from characteristic one")
        XCTAssert(!notRemovedNonExisting, "Should not be able to remove subscribed central from characteristic one")
    }
    
    func testUpdateValueLocalService() {
        let localCharacteristicOne = BluetoothLocalCharacteristic(uuidString: "B170A2CF-8042-4A36-B104-9D8AA75B5095", properties: [], staticValue: nil, permissions: [])
        let localCharacteristicTwo = BluetoothLocalCharacteristic(uuidString: "C7292120-5F58-4D9F-9907-E399C7DFD3D7", properties: [], staticValue: nil, permissions: [])
        localService.addCharacteristic(localCharacteristicOne)
        
        let data = "TesteTeste".dataUsingEncoding(NSUTF8StringEncoding)
        
        let updated = localService.updateValue(data, forCharacteristic: localCharacteristicOne.UUIDString)
        let notUpdated = localService.updateValue(data, forCharacteristic: localCharacteristicTwo.UUIDString)
        XCTAssert(updated, "Should be able to update value for characteristic one")
        XCTAssert(!notUpdated, "Should not be able to update value for characteristic two")
    }

    func testGetCoreBluetoothLocalService() {
        let localCharacteristicOne = BluetoothLocalCharacteristic(uuidString: "B170A2CF-8042-4A36-B104-9D8AA75B5095", properties: [], staticValue: nil, permissions: [])

        localService.addCharacteristic(localCharacteristicOne)
        
        let cbService = localService.CoreBluetoothService
        let cbCharacteristics = cbService.characteristics ?? []
        
        XCTAssert(cbCharacteristics.count == localService.characteristics.count , "Should have the same number of characteristics")
    }
    
    func testEqualLocalServices() {
        let equalService = BluetoothLocalService(uuidString: uuidstring, primaryService: primary)
        let notEqualService = BluetoothLocalService(uuidString : "B170A2CF-8042-4A36-B104-9D8AA75B5095", primaryService : true)
        
        XCTAssert(localService == equalService, "Equal service should be equal to local service")
        XCTAssert(!(localService == notEqualService), "Not Equal service should not be equal to local service")
    }
    
}
