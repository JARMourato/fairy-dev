//
//  BluetoothBlocks.swift
//  JMBluetooth-OSX-Example
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation

//MARK: - General
public typealias StateOfBluetoothChangedBlock = ((state : BluetoothState) -> (Void))?

//MARK: - Central

public typealias FailedToScanForDevicesBlock = ((error : BluetoothError) -> (Void))?
public typealias DiscoveredDevicesBlock = ((discoveredDevices : [BluetoothRemotePeripheral]) -> (Void))?
public typealias FailedToConnectToDeviceBlock = ((peripheral: BluetoothRemotePeripheral?, error : BluetoothError) -> (Void))?
public typealias ConnectedToDeviceBlock = ((peripheral: BluetoothRemotePeripheral) -> (Void))?
public typealias DisconnectedFromDeviceBlock = ((peripheral: BluetoothRemotePeripheral, error : BluetoothError) -> (Void))?
public typealias DiscoveredServicesBlock = ((services : [BluetoothRemoteService]?, error : BluetoothError) -> (Void))?
public typealias DiscoveredCharateristicsBlock = ((characteristics : [BluetoothRemoteCharacteristic]?, error : BluetoothError) -> (Void))?

public typealias ReadCharateristicsBlock = ((data : NSData?, error : BluetoothError) -> (Void))?
public typealias NotifiedCharateristicsBlock = ((data : NSData?, error : BluetoothError) -> (Void))?
public typealias WriteCharateristicsBlock = ((data : NSData?, error : BluetoothError) -> (Void))?

//MARK: - Peripheral

public typealias FailedToStartAdvertisingBlock = ((error : BluetoothError) -> (Void))?
public typealias PeripheralStartAdvertisingBlock = ((peripheral : BluetoothLocalPeripheral) -> (Void))?
