//
//  BluetoothRequest.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

internal struct BluetoothWriteRequest: CustomDebugStringConvertible, CustomStringConvertible {
	
	private let cbattrequest : CBATTRequest
	
	var central : BluetoothRemoteCentral { return BluetoothRemoteCentral(central: cbattrequest.central) }
	var characteristic : BluetoothRemoteCharacteristic { return BluetoothRemoteCharacteristic(characteristic: cbattrequest.characteristic) }
	var offset : Int { return cbattrequest.offset }
	var value : NSData? { return cbattrequest.value }
	var coreBluetoothRequest : CBATTRequest { return cbattrequest }
	
	init(remoteRequest : CBATTRequest) {
		cbattrequest = remoteRequest
	}
	
	var description : String {
		return cbattrequest.description
	}
	
	var debugDescription : String {
		return cbattrequest.debugDescription
	}
}

internal struct BluetoothReadRequest: CustomStringConvertible, CustomDebugStringConvertible  {
	
	private let cbattrequest : CBATTRequest
	
	var central : BluetoothRemoteCentral { return BluetoothRemoteCentral(central: cbattrequest.central) }
	var characteristic : BluetoothRemoteCharacteristic { return BluetoothRemoteCharacteristic(characteristic: cbattrequest.characteristic) }
	var offset : Int { return cbattrequest.offset }
	
	var value : NSData? {
		get {
			return cbattrequest.value
		}
		set {
			cbattrequest.value = newValue
		}
	}
	var coreBluetoothRequest : CBATTRequest { return cbattrequest }
	
	init(remoteRequest : CBATTRequest) {
		cbattrequest = remoteRequest
	}
	
	init(remoteRequest : CBATTRequest, updatedValue : NSData?) {
		cbattrequest = remoteRequest
		cbattrequest.value = updatedValue
	}
	
	var description : String {
		return cbattrequest.description
	}
	
	var debugDescription : String {
		return cbattrequest.debugDescription
	}
}