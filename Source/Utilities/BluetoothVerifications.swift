//
//  BluetoothVerifications.swift
//  Fairy
//
//  Created by iOS on 16/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation

internal class Verify {
    
    static func isUUID(string : String) -> Bool {
        if let _ = NSUUID(UUIDString: string){
            return true
        }
        return false
    }
    
}