//
//  BluetoothLocalCharacteristic.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

internal struct BluetoothLocalCharacteristic: CustomStringConvertible,CustomDebugStringConvertible, BluetoothUUID {
	
	private var cbcharacteristic : CBMutableCharacteristic
	private var centralsNotUpdated : [BluetoothRemoteCentral]
    private var staticValue : Bool = false
	
	var subscribedCentrals : [BluetoothRemoteCentral]
	var UUIDString : String
	var characteristicProperties : [BluetoothCharacteristicProperties]
	var characteristicPermissions : [BluetoothCharacteristicValueAttributePermissions]
	
	var value : NSData? {
        set {
            if !staticValue {
                cbcharacteristic.value = newValue
            }
        }
        get {
            return cbcharacteristic.value
        }
	}
	
	var CoreBluetoothCharacteristic : CBMutableCharacteristic {
		return cbcharacteristic
	}
	
	var description : String {
		return cbcharacteristic.description
	}
	
	var debugDescription : String {
		return cbcharacteristic.debugDescription
	}
	
	//FIXME:
	var centralsNeedingUpdate : [CBCentral]? {
		return centralsNotUpdated.flatMap{$0.CoreBluetoothCentral}
	}
	
	init(uuidString : String, properties : [BluetoothCharacteristicProperties], staticValue : NSData?, permissions : [BluetoothCharacteristicValueAttributePermissions]) {
		
		self.UUIDString = uuidString
		self.characteristicProperties = properties
		self.characteristicPermissions = permissions
		let convertedProperties : CBCharacteristicProperties? = BluetoothCharacteristicProperties.concatMultipleProperties(characteristicProperties)
		
		let convertedPermissions : CBAttributePermissions? = BluetoothCharacteristicValueAttributePermissions.concatMultipleProperties(characteristicPermissions)
		
		cbcharacteristic = CBMutableCharacteristic(type: CBUUID(string: uuidString), properties: convertedProperties ?? [], value: staticValue, permissions: convertedPermissions ?? [.Readable,.Writeable])
        
        self.staticValue = staticValue != nil ? true : false
        self.centralsNotUpdated = []
        self.subscribedCentrals = []
	}
	
	//MARK: Search centrals
	
	func isSubscribedCentral(central : BluetoothRemoteCentral) -> Bool{
		guard subscribedCentrals.count > 0 else { return false }
		
		for index in 0..<subscribedCentrals.count {
			if subscribedCentrals[index].identifier == central.identifier {
				return true
			}
		}
		return false
	}
	
	//MARK: Add & Remove centrals to characteristic
	
	mutating func updateCharacteristic(subscribedCentrals centrals : [BluetoothRemoteCentral]?) {
		subscribedCentrals = centrals ?? []
	}
	
	mutating func addSubscribedCentral(central : BluetoothRemoteCentral) -> Bool {
        if isSubscribedCentral(central) { return false }
		subscribedCentrals.append(central)
        return true
	}
	
	mutating func removeSubscribedCentral(central : BluetoothRemoteCentral) -> Bool{
        guard let found = subscribedCentrals.indexOf({$0.identifier == central.identifier}) else { return false }
        subscribedCentrals.removeAtIndex(found)
		removeUpdatedCentral(central)
		return true
	}
	
	mutating func addNotUpdatedCentral(central : BluetoothRemoteCentral) -> Bool{
        guard centralsNotUpdated.indexOf({$0.identifier == central.identifier}) == nil else { return false }
		centralsNotUpdated.append(central)
        return true
	}
	
	mutating func removeUpdatedCentral(central : BluetoothRemoteCentral) -> Bool{
        guard let found = centralsNotUpdated.indexOf({$0.identifier == central.identifier}) else { return false }
        centralsNotUpdated.removeAtIndex(found)
        return true
	}
}

extension BluetoothLocalCharacteristic: Equatable {}

internal func ==(lhs: BluetoothLocalCharacteristic, rhs: BluetoothLocalCharacteristic) -> Bool{
    return lhs.UUIDString == rhs.UUIDString
    
    //    guard lhs.UUIDString == rhs.UUIDString else { return false }
    //    guard lhs.primary == rhs.primary else { return false }
    //
    //
    //    return true
}
