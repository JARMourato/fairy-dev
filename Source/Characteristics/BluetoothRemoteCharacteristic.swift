//
//  BluetoothRemoteCharacteristic.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

public class BluetoothRemoteCharacteristic: NSObject, BluetoothUUID {
    
    private var cbcharacteristic: CBCharacteristic
    
    var UUIDString: String {
        get {
            return stringFrom(cbcharacteristic.UUID)
        }
    }
    
    var service: BluetoothRemoteService {
        return BluetoothRemoteService(service: cbcharacteristic.service)
    }
    var properties: [BluetoothCharacteristicProperties] {
        return BluetoothCharacteristicProperties.convertToBluetoothCharacteristicProperties(cbcharacteristic.properties)
    }
    
    var value: NSData? {
        return cbcharacteristic.value
    }
    
    var descriptors: [CBDescriptor]? {
        return cbcharacteristic.descriptors
    }
    
    var isNotifying: Bool {
        return cbcharacteristic.isNotifying
    }
    
    var CoreBluetoothCharacteristic: CBCharacteristic {
        return cbcharacteristic
    }
    
    init(characteristic: CBCharacteristic) {
        cbcharacteristic = characteristic
    }
    
    public override var description: String {
        return cbcharacteristic.description
    }
    
    public override var debugDescription: String {
        return cbcharacteristic.debugDescription
    }
}
