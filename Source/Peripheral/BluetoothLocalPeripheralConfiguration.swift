//
//  BluetoothLocaPeripheralConfiguration.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

public class BluetoothLocalPeripheralConfiguration: NSObject {
	
	public var runOnMainThread : Bool = true
	public var peripheralVerbosity : Verbosity = .Debug
	public var peripheralName : String = "Fairy Peripheral"
	//MARK: Delegate
	public var delegate : BluetoothLocalPeripheralDelegate? = nil
	//MARK: Blocks
	public var stateChangedBlock : StateOfBluetoothChangedBlock = nil
	public var bluetoothFailedToStartAdvertising : FailedToStartAdvertisingBlock = nil
	public var bluetoothSuccessStartAdvertising : PeripheralStartAdvertisingBlock = nil
	//MARK: Flags
	public var bypassReadWriteRequests : Bool = false
	public var onlyAllowAuthorizedDevices : Bool = false
	public var alwaysUpdateImediatiallyAfterCharacteristicSubscription : Bool = true
	//public var shouldNotifyCharacteristicUpdateAfterSuccessfullWrite : Bool = false
	//MARK: Array of services
	internal var bluetoothServices : [BluetoothLocalService] = []
}

