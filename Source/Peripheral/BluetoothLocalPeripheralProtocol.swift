//
//  BluetoothLocalPeripheralProtocol.swift
//  JMBluetooth-OSX-Example
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation

@objc public protocol BluetoothLocalPeripheralDelegate {
	
	optional func bluetoothStateChanged(state : BluetoothState)
	
	optional func failedToStartAdvertisingPeripheral(error : BluetoothError)
	optional func peripheralStartedAdvertising(peripheral : BluetoothLocalPeripheral)
	
	optional func peripheralDidAddService(service: String, withError: String?)
	
	optional func peripheralAllowSubscribing(fromCentralWithIdenfifier identifier: String, onCharacteristicWithUUID characteristicUUID : String) -> Bool
	optional func peripheralAllowImediateUpdateOnSubcription(fromCentralWithIdenfifier identifier: String, onCharacteristicWithUUID characteristicUUID : String) -> Bool
	
	optional func peripheralShouldFullfillReadRequest(fromCentralWithIdenfifier identifier: String, onCharacteristicWithUUID characteristicUUID : String) -> Bool
	optional func peripheralShouldFullfillWriteRequest(fromCentralWithIdenfifier identifier: String, onCharacteristicWithUUID characteristicUUID : String, withData writeData : NSData?) -> Bool
	
	optional func peripheralShouldBypassReadRequest(fromCentralWithIdenfifier identifier: String, onCharacteristicWithUUID characteristicUUID : String) -> NSData?
	optional func peripheralShouldBypassWriteRequest(fromCentralWithIdenfifier identifier: String, onCharacteristicWithUUID characteristicUUID : String, withData writeData : NSData?) -> NSData?
	
	optional func peripheralCompletedReceivedWriteRequest(fromCentralWithIdenfifier identifier: String, onCharacteristicWithUUID characteristicUUID : String, writeData : NSData?)
}
