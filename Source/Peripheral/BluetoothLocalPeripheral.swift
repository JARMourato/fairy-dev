//
//  BluetoothLocalPeripheral.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import CoreBluetooth

#if os(OSX)
	import AppKit;
#elseif os(iOS)
	import UIKit;
#endif

public class BluetoothLocalPeripheral : NSObject {
	
	//MARK: - Peripheral Manager and Delegate Proxy
	internal var manager : CBPeripheralManager? = nil
	internal var delegateProxy : PeripheralManagerDelegateProxy?
	
	//MARK: - Bluetooth Local Peripheral Delegate
	public var delegate : BluetoothLocalPeripheralDelegate?
	
	//MARK: - Verbosity
	var verbosity : Verbosity = .Off
	
	//MARK: - Blocks
	//internal var bluetoothChangedStateBlock : StateOfBluetoothChangedBlock = nil
	internal var bluetoothFailedToStartAdvertising : FailedToStartAdvertisingBlock = nil
	internal var bluetoothSuccessStartAdvertising : PeripheralStartAdvertisingBlock = nil
	
	//MARK: - Flags
	internal var peripheralManagerIsReady : Bool = false
	public var isAdvertising : Bool = false
	
	//MARK: - Peripheral properties
	//var numberOfPossibleConnectedDevices : Int = 1
	internal var peripheralConfiguration : BluetoothLocalPeripheralConfiguration?
	
	internal override init() {
		super.init()
		delegateProxy = PeripheralManagerDelegateProxy(delegate: self) 
	}
}
