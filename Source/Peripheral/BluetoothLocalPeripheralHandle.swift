//
//  BluetoothLocalPeripheralHandle.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothLocalPeripheral: PeripheralManagerProxyDelegate {
	
	internal func peripheralManagerDidUpdateState(state: BluetoothState) {
		
		peripheralManagerIsReady = false
		
		switch (state) {
		case .PoweredOn:
			peripheralManagerIsReady = true
		default: break
		}
		
		if !peripheralManagerIsReady && isAdvertising {
			stopAdvertising()
		}
		
		peripheralConfiguration?.stateChangedBlock?(state: state)
		delegate?.bluetoothStateChanged?(state)
		
		verbosity.INFO("Bluetooth State is \(state.description)")
	}
	
	internal func peripheralManager(didAddService service : BluetoothRemoteService, error: (bluetoothError : BluetoothError, errorDescription : String)?) {
		let message : String = "\n---------------Add Service Result--------------------\n" +
		"\(self)\n\(service.debugDescription)\n\(error.debugDescription)\n" +
		"-----------------------------------------------------\n"
		
		delegate?.peripheralDidAddService?(service.UUIDString, withError: error?.errorDescription)
		verbosity.DEBUG(message)
	}
	
	internal func peripheralManagerDidStartAdvertising(error: (bluetoothError: BluetoothError, errorDescription: String)?) {
		if let err = error {
			verbosity.ERROR("Advertisement failed with error \(err)\n")
			verbosity.INFO("Failed to start advertising\n")
			
			bluetoothFailedToStartAdvertising?(error: .ErrorOnStartingAdvertisement)
			delegate?.failedToStartAdvertisingPeripheral?(.ErrorOnStartingAdvertisement)
			
			isAdvertising = false
			manager?.stopAdvertising()
		} else {
			verbosity.DEBUG("Advertisement started on peripheral \(peripheralConfiguration?.peripheralName)\n")
			verbosity.INFO("Peripheral started advertising\n")
			
			bluetoothSuccessStartAdvertising?(peripheral: self)
			delegate?.peripheralStartedAdvertising?(self)
		}
	}
	
	internal func peripheralManagerDidReceiveReadRequest(var request: BluetoothReadRequest) {
		
		verbosity.DEBUG("Read Request:\(request.debugDescription)")
		
		//Request validation
		
		guard let findResult = peripheralConfiguration?.peripheralHas(characteristic: request.characteristic.UUID, forService: request.characteristic.service.UUID) else {
			verbosity.ERROR("Peripheral configuration is nil")
			return
		}

		guard findResult.exists else {
			verbosity.DEBUG("Characteristic or service not found. Request:\(request.debugDescription)")
			manager?.respondToRequest(request.coreBluetoothRequest, withResult: CBATTError.AttributeNotFound)
			return
		}
		
		guard let foundCharacteristic = findResult.characteristic else {
			verbosity.ERROR("Error on method > peripheralHas(characteristic characteristicUUID : CBUUID, forService serviceUUID : CBUUID)\n")
			return
		}
		
		guard request.offset < foundCharacteristic.value?.length else {
			manager?.respondToRequest(request.coreBluetoothRequest, withResult: CBATTError.InvalidOffset)
			return
		}
		
		//Request aproval beyond this point
		
		if peripheralConfiguration!.bypassReadWriteRequests {
			if let bypassData = delegate?.peripheralShouldBypassReadRequest?(fromCentralWithIdenfifier: request.central.UUIDString, onCharacteristicWithUUID: request.characteristic.UUIDString) {
				request.value = bypassData
				manager?.respondToRequest(request.coreBluetoothRequest, withResult: CBATTError.Success)
				verbosity.INFO("Read Request From Device device \(request.central.identifier) bypassed with data :\(bypassData)\n ")
				return
			}
		}
		
		let value = (foundCharacteristic.value != nil) ? foundCharacteristic.value?.subdataWithRange(NSMakeRange(request.offset, (foundCharacteristic.value?.length)! - request.offset)) : nil
		
		if peripheralConfiguration!.onlyAllowAuthorizedDevices {
			if foundCharacteristic.isSubscribedCentral(request.central) {
				request.value = value
				manager?.respondToRequest(request.coreBluetoothRequest, withResult: CBATTError.Success)
				verbosity.INFO("Read Request Approved : Authorized Device \(request.central.identifier)")
			}
			else {
				manager?.respondToRequest(request.coreBluetoothRequest, withResult: CBATTError.ReadNotPermitted)
				verbosity.INFO("Read Request denied : Unauthorized Device \(request.central.identifier)")
			}
			return
		}
		
		if let approved = delegate?.peripheralShouldFullfillReadRequest?(fromCentralWithIdenfifier: request.central.UUIDString, onCharacteristicWithUUID: request.characteristic.UUIDString) {
			if approved {
				request.value = value
				manager?.respondToRequest(request.coreBluetoothRequest, withResult: CBATTError.Success)
				verbosity.INFO("Read Request Approved By Peripheral Delegate")
			} else {
				manager?.respondToRequest(request.coreBluetoothRequest, withResult: CBATTError.ReadNotPermitted)
				verbosity.INFO("Read Request Denied By Peripheral Delegate")
			}
			return
		}
		
		request.value = value
		manager?.respondToRequest(request.coreBluetoothRequest, withResult: CBATTError.Success)
		verbosity.INFO("Unrestricted Read Request Complete")
		
	}
	
	internal func peripheralManagerDidReceiveWriteRequests(requests: [BluetoothWriteRequest]) {
		
		for request in requests {
			
			verbosity.DEBUG("Write Request:\(request.debugDescription)")
			
			// Request Validation
			
			guard let findResult = peripheralConfiguration?.peripheralHas(characteristic: request.characteristic.UUID, forService: request.characteristic.service.UUID) else {
				verbosity.ERROR("Peripheral configuration is nil")
				continue
			}
			
			guard findResult.exists else {
				verbosity.DEBUG("Characteristic or service not found. Request:\(request.debugDescription)")
				manager?.respondToRequest(request.coreBluetoothRequest, withResult: CBATTError.AttributeNotFound)
				continue
			}
			
			guard let foundCharacteristic = findResult.characteristic else {
				verbosity.ERROR("Error on method > peripheralHas(characteristic characteristicUUID : CBUUID, forService serviceUUID : CBUUID)\n")
				continue
			}
			
			guard foundCharacteristic.characteristicProperties.contains(.Write) else {
				verbosity.DEBUG("Request to write to non writable characteristic \(foundCharacteristic.debugDescription)\n")
				continue
			}
			
			// Request Aproval
			let newValue = request.value
			
			if peripheralConfiguration!.bypassReadWriteRequests {
				if let bypassData = delegate?.peripheralShouldBypassWriteRequest?(fromCentralWithIdenfifier: request.central.UUIDString, onCharacteristicWithUUID: request.characteristic.UUIDString, withData: newValue) {
					manager?.respondToRequest(request.coreBluetoothRequest, withResult: CBATTError.Success)
					peripheralConfiguration?.updateCharacteristic(request.characteristic.UUID, forService: request.characteristic.service.UUID, withNewValue: bypassData)
					verbosity.INFO("Write Request From Device device \(request.central.identifier) bypassed with data :\(bypassData)\n ")
					return
				}
			}
			
			if peripheralConfiguration!.onlyAllowAuthorizedDevices {
				if foundCharacteristic.isSubscribedCentral(request.central) {
					manager?.respondToRequest(request.coreBluetoothRequest, withResult: CBATTError.Success)
					peripheralConfiguration?.updateCharacteristic(request.characteristic.UUID, forService: request.characteristic.service.UUID, withNewValue: newValue)
					verbosity.INFO("Write Request Approved : Authorized Device \(request.central.identifier)")
				}
				else {
					manager?.respondToRequest(request.coreBluetoothRequest, withResult: CBATTError.WriteNotPermitted)
					verbosity.INFO("Write Request Denied : Unauthorized Device \(request.central.identifier)")
				}
				return
			}
			
			if let approved = delegate?.peripheralShouldFullfillWriteRequest?(fromCentralWithIdenfifier: request.central.UUIDString, onCharacteristicWithUUID: request.characteristic.UUIDString, withData: newValue) {
				if approved {
					peripheralConfiguration?.updateCharacteristic(request.characteristic.UUID, forService: request.characteristic.service.UUID, withNewValue: newValue)
					manager?.respondToRequest(request.coreBluetoothRequest, withResult: CBATTError.Success)
					verbosity.INFO("Write Request Approved By Peripheral Delegate")
				} else {
					manager?.respondToRequest(request.coreBluetoothRequest, withResult: CBATTError.WriteNotPermitted)
					verbosity.INFO("Write Request Denied By Peripheral Delegate")
				}
				return
			}
			
			peripheralConfiguration?.updateCharacteristic(request.characteristic.UUID, forService: request.characteristic.service.UUID, withNewValue: newValue)
			manager?.respondToRequest(request.coreBluetoothRequest, withResult: CBATTError.Success)
			delegate?.peripheralCompletedReceivedWriteRequest?(fromCentralWithIdenfifier: request.central.UUIDString, onCharacteristicWithUUID: request.characteristic.UUIDString, writeData: newValue)
			verbosity.INFO("Unrestricted Write Request Complete")
		}
	}
	
	internal func peripheralManager(central central: BluetoothRemoteCentral, didSubscribeToCharacteristic characteristic: BluetoothRemoteCharacteristic) {
		verbosity.INFO("Central \(central) subscribed characteristic \(characteristic)\n")
		
		guard let findCharacteristic = peripheralConfiguration?.peripheralHas(characteristic: characteristic.UUID, forService: characteristic.service.UUID) else {
			verbosity.ERROR("Peripheral configuration is nil")
			return
		}
		
		guard findCharacteristic.exists else { verbosity.DEBUG("CBCentral \(central) tried to subscribe non existing characteristic\n") ; return }
		
		guard let foundCharacteristic = findCharacteristic.characteristic else {
			verbosity.ERROR("Error on method > peripheralHas(characteristic characteristicUUID : CBUUID, forService serviceUUID : CBUUID)\n")
			return
		}
		
		guard foundCharacteristic.characteristicProperties.contains(.Notify) else {
			verbosity.DEBUG("Central \(central) tried to subscribe non notifiable characteristic \(foundCharacteristic)\n")
			return
		}
		
		guard let localcharacteristic = findCharacteristic.characteristic else {
			verbosity.DEBUG("There was an unknown error \(findCharacteristic)\n")
			return
		}
		let updatedValue = characteristic.value
		
		if peripheralConfiguration!.onlyAllowAuthorizedDevices {
			guard let authorizedDevice = delegate?.peripheralAllowSubscribing?(fromCentralWithIdenfifier: central.identifier.UUIDString, onCharacteristicWithUUID: characteristic.UUIDString) where authorizedDevice == true else {
				verbosity.INFO("Device not authorized")
				return
			}
		}
		peripheralConfiguration!.addSubscribedCentral(central, inCharacteristic: localcharacteristic, ofService: characteristic.service.UUID)
		
		var shouldUpdate : Bool = false
		
		let delegateAllows = delegate?.peripheralAllowImediateUpdateOnSubcription?(fromCentralWithIdenfifier: central.identifier.UUIDString, onCharacteristicWithUUID: characteristic.UUIDString)
		
		if let updateAllowed = delegateAllows where updateAllowed == true{
			shouldUpdate = true
		} else if peripheralConfiguration!.alwaysUpdateImediatiallyAfterCharacteristicSubscription {
			shouldUpdate = true
		}
		
		if shouldUpdate {
			let didSendValue = manager?.updateValue(updatedValue ?? NSData(), forCharacteristic: localcharacteristic.CoreBluetoothCharacteristic, onSubscribedCentrals: [central.CoreBluetoothCentral])
			if let sent = didSendValue where sent == false {
				peripheralConfiguration?.addNotUpdatedCentral(central, inCharacteristic: localcharacteristic, ofService: characteristic.service.UUID)
				verbosity.DEBUG("Device authorized and but failed to notify with data: \(updatedValue ?? NSData())")
			} else {
				verbosity.DEBUG("Device authorized and notified with data : \(updatedValue ?? NSData())")
			}
		} else {
			verbosity.DEBUG("Device authorized but not updated right away")
		}
	}
	
	internal func peripheralManager(central central: BluetoothRemoteCentral, didUnsubscribeFromCharacteristic characteristic: BluetoothRemoteCharacteristic) {
		verbosity.INFO("\(central) unsubscribed characteristic \(characteristic)\n")
		
		guard let findCharacteristic = peripheralConfiguration?.peripheralHas(characteristic: characteristic.UUID, forService: characteristic.service.UUID) else {
			verbosity.ERROR("Peripheral configuration is nil")
			return
		}
		
		guard findCharacteristic.exists else { verbosity.DEBUG("Central \(central) tried to unsubscribe from non existing characteristic\n"); return }
		
		guard let foundCharacteristic = findCharacteristic.characteristic else {
			verbosity.DEBUG("Error on method > peripheralHas(characteristic characteristicUUID : CBUUID, forService serviceUUID : CBUUID)\n")
			return
		}

		guard foundCharacteristic.characteristicProperties.contains(.Notify) else {
			verbosity.DEBUG("Central \(central) tried to unsubscribe from non notifiable characteristic \(foundCharacteristic)\n")
			return
		}
		
		guard let localCharacteristic = findCharacteristic.characteristic else {
			if verbosity == .Debug { print("Debug: There was an unknown error \(findCharacteristic)\n") }
			return
		}
		
		if peripheralConfiguration!.onlyAllowAuthorizedDevices {
			if foundCharacteristic.isSubscribedCentral(central) {
				peripheralConfiguration?.removeSubscribedCentral(central, inCharacteristic: localCharacteristic, ofService: characteristic.service.UUID)
			}
		} else {
			peripheralConfiguration?.removeSubscribedCentral(central, inCharacteristic: localCharacteristic, ofService: characteristic.service.UUID)
		}
	}
	
	internal func peripheralManagerIsReadyToUpdateSubscribers() {
		verbosity.DEBUG("Called is ready to update subscribers")
		
	}
	
	internal func peripheralManagerWillRestoreState(dict: [String : AnyObject]) {
		verbosity.DEBUG("Called will restore state")
	}
}
