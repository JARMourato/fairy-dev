//
//  BluetoothLocalPeripheralConfigurationServices.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

public extension BluetoothLocalPeripheralConfiguration {
	
	public func peripheralServices() -> [CBMutableService] { return bluetoothServices.flatMap{ $0.CoreBluetoothService } }
	
	public func addServiceWith(uuidString uuidString : String, primaryService : Bool) -> Bool{
		let repeatedService : Int? = bluetoothServices.indexOf{ $0.UUIDString == uuidString }
		
		guard repeatedService == nil else { peripheralVerbosity.INFO("Service with the uuid provided already exists\n"); return false }
		
		let debugMessage = "Peripheral services were : \(bluetoothServices)\n" + "Will try to add service with uuid \(uuidString) of type \(primaryService ? "Primary" : "Secondary" )\n"
		peripheralVerbosity.DEBUG(debugMessage)
		
		let bluetoothService : BluetoothLocalService = BluetoothLocalService(uuidString: uuidString, primaryService: primaryService)
		
		peripheralVerbosity.DEBUG("Created service : \(bluetoothService.debugDescription)\n")
		
		bluetoothServices.append(bluetoothService)
		
		peripheralVerbosity.DEBUG("Debug: Peripheral services are now : \(bluetoothServices)\n")
		peripheralVerbosity.INFO("Info: New service with uuid \(uuidString) of type \(primaryService ? "Primary" : "Secondary" ) was added to the peripheral\n")
		
		return true
	}
	
	public func removeServiceWith(uuidString uuidString : String) -> Bool {
		guard bluetoothServices.count > 0 else { peripheralVerbosity.INFO("Info: No services removed since peripheral has no services\n");  return false}
		
		let debugMessage = "Peripheral services were : \(bluetoothServices)\n" + "Will try to remove service with uuid \(uuidString)\n"
		peripheralVerbosity.DEBUG(debugMessage)
		
		if let index = bluetoothServices.indexOf({$0.UUIDString == uuidString}) {
			bluetoothServices.removeAtIndex(index)
		}
		else {
			if peripheralVerbosity == .Debug { print("Debug: No services with uuid : \(uuidString) were found\n") }
			return false
		}
		
		peripheralVerbosity.DEBUG("Debug: Peripheral services are now : \(bluetoothServices)\n")
		peripheralVerbosity.INFO("Info: Service with uuid \(uuidString) removed from peripheral\n")
		
		return true
	}
}