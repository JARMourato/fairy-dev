//
//  BluetoothLocalPeripheralConfigurationSubscribing.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth


internal extension BluetoothLocalPeripheralConfiguration {
	
	//MARK: Centrals subscribing characteristics
	
	func addSubscribedCentral(central : BluetoothRemoteCentral, inCharacteristic characteristic : BluetoothLocalCharacteristic, ofService serviceCBUUID : CBUUID) {
		let debugMessage = "Adding subscribed central operation\n" +
		"Characteristic with \(characteristic.debugDescription) of service \(serviceCBUUID.debugDescription) HAD the following subscribed centrals \(characteristic.subscribedCentrals.debugDescription) \n"
		
		peripheralVerbosity.DEBUG(debugMessage)
		
		for index in 0..<bluetoothServices.count {
			if bluetoothServices[index].UUID == serviceCBUUID {
				bluetoothServices[index].addSubscribedCentral(central, forCharacteristic: characteristic)
				break
			}
		}
		peripheralVerbosity.DEBUG("Characteristic with \(characteristic.debugDescription) of service \(serviceCBUUID.debugDescription) NOW HAS the following subscribed centrals \(characteristic.subscribedCentrals.debugDescription) \n")
	}
	
	func removeSubscribedCentral(central : BluetoothRemoteCentral, inCharacteristic characteristic : BluetoothLocalCharacteristic, ofService serviceCBUUID : CBUUID) {
		let debugMessage = "Removing subscribed central operation\n" +
		"Characteristic with \(characteristic.debugDescription) of service \(serviceCBUUID.debugDescription) HAD the following subscribed centrals \(characteristic.subscribedCentrals.debugDescription)\n"
		
		peripheralVerbosity.DEBUG(debugMessage)

		for index in 0..<bluetoothServices.count {
			if bluetoothServices[index].UUID == serviceCBUUID {
				bluetoothServices[index].removeSubscribedCentral(central, forCharacteristic: characteristic)
				break
			}
		}
		peripheralVerbosity.DEBUG("Characteristic with \(characteristic.debugDescription) of service \(serviceCBUUID.debugDescription) NOW HAS the following subscribed centrals \(characteristic.subscribedCentrals.debugDescription) \n")
	}
	
	func addNotUpdatedCentral(central : BluetoothRemoteCentral, inCharacteristic characteristic : BluetoothLocalCharacteristic, ofService serviceCBUUID : CBUUID) {
		let debugMessage = "Adding not updated central operation\n" +
		"Characteristic with \(characteristic.debugDescription) of service with UUID \(serviceCBUUID.debugDescription) HAD the following centrals needing update \(characteristic.centralsNeedingUpdate)\n"
		
		peripheralVerbosity.DEBUG(debugMessage)
		
		for index in 0..<bluetoothServices.count {
			if bluetoothServices[index].UUID == serviceCBUUID {
				bluetoothServices[index].addNotUpdatedSubscribedCentral(central, forCharacteristic: characteristic)
				break
			}
		}
		peripheralVerbosity.DEBUG("Characteristic with \(characteristic.debugDescription) of service with UUID \(serviceCBUUID.debugDescription) NOW HAS the following centrals needing update \(characteristic.centralsNeedingUpdate) \n")
	}
	
	func removeUpdatedCentral(central : BluetoothRemoteCentral, inCharacteristic characteristic : BluetoothLocalCharacteristic, ofService serviceCBUUID : CBUUID) {
		let debugMessage = "Removing not updated central operation\n" +
		"Characteristic with \(characteristic.debugDescription) of service with UUID \(serviceCBUUID.debugDescription) HAD the following centrals needing update \(characteristic.centralsNeedingUpdate) \n"
		
		peripheralVerbosity.DEBUG(debugMessage)
		
		for index in 0..<bluetoothServices.count {
			if bluetoothServices[index].UUID == serviceCBUUID {
				bluetoothServices[index].removeUpdatedCentral(central, forCharacteristic: characteristic)
				break
			}
		}
		peripheralVerbosity.DEBUG("Characteristic with \(characteristic.debugDescription) of service with UUID \(serviceCBUUID.debugDescription) NOW HAS the following centrals needing update \(characteristic.centralsNeedingUpdate) \n") 
	}
}