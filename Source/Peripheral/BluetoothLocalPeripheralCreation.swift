//
//  BluetoothLocalPeripheralCreation.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothLocalPeripheral {
	
	static public func createPeripheral(configuration : BluetoothLocalPeripheralConfiguration) -> BluetoothLocalPeripheral{
		
		let localPeripheral : BluetoothLocalPeripheral = BluetoothLocalPeripheral()
		
		var backgroundQueue : dispatch_queue_t? = nil
		
		#if os(iOS)
			backgroundQueue = configuration.runOnMainThread ? nil : dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
		#elseif os(OSX)
			if #available(OSX 10.10, *) {
				backgroundQueue = configuration.runOnMainThread ? nil : dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)
			} else {
				backgroundQueue = configuration.runOnMainThread ? nil : dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
			}
		#endif
		
		localPeripheral.manager = CBPeripheralManager(delegate: localPeripheral.delegateProxy, queue: backgroundQueue, options: nil)
		
		
		//localPeripheral.bluetoothChangedStateBlock = configuration.stateChangedBlock
		localPeripheral.delegate = configuration.delegate
		localPeripheral.verbosity = configuration.peripheralVerbosity
		
		localPeripheral.peripheralConfiguration = configuration
		localPeripheral.publishServices(configuration.bluetoothServices)
		
		return localPeripheral
	}
	
	// TODO:  TBD...
	
	private func publishServices(services : [BluetoothLocalService]) {
		for service in services {
			manager?.addService(service.CoreBluetoothService)
		}
	}
	
	private func unPublishServices() {
		manager?.removeAllServices()
	}
	
	private func deletePeripheral() {
		if isAdvertising {
			manager?.stopAdvertising()
		}
		manager?.removeAllServices()
		manager = nil
	}
}

