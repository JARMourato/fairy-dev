//
//  BluetoothLocalPeripheralConfigurationSearching.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

internal extension BluetoothLocalPeripheralConfiguration {
	
	func findService(withServiceUUID serviceUUID : CBUUID) -> BluetoothLocalService? {
		if let found = bluetoothServices.indexOf({ $0.UUID == serviceUUID}) {
			peripheralVerbosity.DEBUG("Found service with uuid : \(serviceUUID.debugDescription) - \(bluetoothServices[found])\n")
			return bluetoothServices[found]
		} else {
			peripheralVerbosity.DEBUG("No service with uuid : \(serviceUUID.debugDescription) found\n")
			return nil
		}
	}
	
	func findCharacteristic(withCharacteristicUUID characteristicUUID : CBUUID, andService service : BluetoothLocalService) -> BluetoothLocalCharacteristic?{
		
		guard service.characteristics.count > 0 else {
			peripheralVerbosity.DEBUG("Service : \(service.debugDescription) has no characteristics\n")
			return nil
		}
		if let foundCharacteristic = service.hasCharacteristic(characteristicUUID) {
			peripheralVerbosity.DEBUG("Found characteristic with uuid : \(characteristicUUID.debugDescription) - \(foundCharacteristic.debugDescription) in service : \(service.debugDescription)\n")
			return foundCharacteristic
		}
		else {
			peripheralVerbosity.DEBUG("Service : \(service.debugDescription) has has no characteristic with uuid : \(characteristicUUID.debugDescription)\n")
			return nil
		}
	}
	
	func findCharacteristics(withCharacteristicUUID characteristicUUID : CBUUID) -> [BluetoothLocalCharacteristic]? {
		guard bluetoothServices.count > 0 else { peripheralVerbosity.DEBUG("Peripheral has no services\n"); return nil }
		
		var foundCharacteristics : [BluetoothLocalCharacteristic] = []
		
		for service in bluetoothServices {
			if let characteristic = findCharacteristic(withCharacteristicUUID: characteristicUUID, andService: service) {
				foundCharacteristics.append(characteristic)
			}
		}
		
		guard foundCharacteristics.count > 0 else { peripheralVerbosity.DEBUG("No characteristics with uuid : \(characteristicUUID.debugDescription) found\n"); return nil }
		
		peripheralVerbosity.DEBUG("Found characteristics with uuid : \(characteristicUUID.debugDescription) - \(foundCharacteristics.debugDescription) \n")
		
		return foundCharacteristics
	}
	
	func peripheralHas(service serviceUUID : CBUUID) -> (exists : Bool, service : BluetoothLocalService?) {
		guard let service = findService(withServiceUUID: serviceUUID) else { return (false, nil) }
		return (true, service)
	}
	
	func peripheralHas(characteristic characteristicUUID : CBUUID, forService serviceUUID : CBUUID) -> (exists : Bool, characteristic : BluetoothLocalCharacteristic?){
		guard let service = findService(withServiceUUID: serviceUUID) else { return (false,nil) }
		guard let characteristic = findCharacteristic(withCharacteristicUUID: characteristicUUID, andService: service) else { return (false,nil) }
		return (true,characteristic)
	}
	
	func peripheralHas(characteristic characteristicUUID : CBUUID) -> (exists : Bool, characteristics : [BluetoothLocalCharacteristic]?) {
		guard let characteristics = findCharacteristics(withCharacteristicUUID: characteristicUUID) else {return (false, nil)}
		return (true,characteristics)
	}
	
}
