//
//  BluetoothLocalPeripheralConfigurationCharacteristics.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

public extension BluetoothLocalPeripheralConfiguration {
	
	// For the time being forcing permissions..
	//MARK: For objc...
	public func addCharacteristicWith(
		characteristicUUID characteristicUUID : String,
		serviceUUID: String,
		characteristicProperties : [Int]
		)-> Bool {
			let forcedPermissions : [Int] = [BluetoothCharacteristicValueAttributePermissions.Readable.rawValue, BluetoothCharacteristicValueAttributePermissions.Writeable.rawValue]
		return addCharacteristicWith(characteristicUUID: characteristicUUID, serviceUUID: serviceUUID, characteristicProperties: characteristicProperties, characteristicPermissions: forcedPermissions)
	}
	
	private func addCharacteristicWith(
		characteristicUUID characteristicUUID : String,
		serviceUUID: String,
		characteristicProperties : [Int],
		characteristicPermissions : [Int]
		) -> Bool {
			
			let flattenProperties = characteristicProperties.flatMap{ BluetoothCharacteristicProperties(rawValue: $0) }
			let flattenPermissions = characteristicPermissions.flatMap{ BluetoothCharacteristicValueAttributePermissions(rawValue: $0) }
			
			let debugMessage = "Properties conversion \(characteristicProperties) -> \(flattenProperties)\n" + "Permissions conversion \(characteristicPermissions) -> \(flattenPermissions)\n"
			peripheralVerbosity.DEBUG(debugMessage)

			return addCharacteristic(characteristicUUID: characteristicUUID, serviceUUID: serviceUUID, characteristicProperties: flattenProperties, characteristicPermissions: flattenPermissions)
	}
	
	public func addCharacteristic(
		characteristicUUID characteristicUUID : String,
		serviceUUID: String,
		characteristicProperties : [BluetoothCharacteristicProperties]
		) -> Bool {
		let forcedPermissions : [BluetoothCharacteristicValueAttributePermissions] = [BluetoothCharacteristicValueAttributePermissions.Readable, BluetoothCharacteristicValueAttributePermissions.Writeable]
		return addCharacteristic(characteristicUUID: characteristicUUID, serviceUUID: serviceUUID, characteristicProperties: characteristicProperties, characteristicPermissions: forcedPermissions)
	}
	
	private func addCharacteristic(
		characteristicUUID characteristicUUID : String,
		serviceUUID: String,
		characteristicProperties : [BluetoothCharacteristicProperties],
		characteristicPermissions : [BluetoothCharacteristicValueAttributePermissions]
		) -> Bool {
			
			guard bluetoothServices.count > 0 else { peripheralVerbosity.INFO("Peripheral has no services\n") ; return false }
			
			peripheralVerbosity.DEBUG("Will try to add characteristic with uuid \(characteristicUUID) to service with uuid \(serviceUUID)\n")
			
			guard let indexOfServiceWithSpecifiedUUID = bluetoothServices.indexOf({ $0.UUIDString == serviceUUID}) else {
				peripheralVerbosity.DEBUG("Service with : \(serviceUUID) not found on peripheral\n")
				peripheralVerbosity.INFO("Characteristic with uuid \(characteristicUUID) was NOT added to service with uuid \(serviceUUID)\n")
				return false
			}
			
			guard bluetoothServices[indexOfServiceWithSpecifiedUUID].hasCharacteristic(characteristicUUID) == nil else {
				peripheralVerbosity.DEBUG("Characteristic with : \(characteristicUUID) already exists in service \(bluetoothServices[indexOfServiceWithSpecifiedUUID].debugDescription)\n")
				peripheralVerbosity.INFO("Characteristic with uuid \(characteristicUUID) was NOT added to service with uuid \(serviceUUID)\n")
				return false
			}
			
			let bluetoothCharacteristic = BluetoothLocalCharacteristic(uuidString: characteristicUUID, properties: characteristicProperties, staticValue: nil, permissions: characteristicPermissions)
			
			peripheralVerbosity.DEBUG("Created charactheristic : \(bluetoothCharacteristic.debugDescription)\n")
			peripheralVerbosity.DEBUG("Service characteristics were: \(bluetoothServices[indexOfServiceWithSpecifiedUUID].characteristics)\n")
			
			bluetoothServices[indexOfServiceWithSpecifiedUUID].addCharacteristic(bluetoothCharacteristic)
			
			peripheralVerbosity.DEBUG("Service characteristics are now : \(bluetoothServices[indexOfServiceWithSpecifiedUUID].characteristics)\n")
			peripheralVerbosity.INFO("Characteristic with uuid \(characteristicUUID) was added to service with uuid \(serviceUUID)\n")
			
			return true
	}
	
	public func removeCharacteristicWith(characteristicUUID characteristicUUID : String, serviceUUID: String) -> Bool {
		guard bluetoothServices.count > 0 else { peripheralVerbosity.INFO("Info: Peripheral has no services\n"); return false }
		
		guard let indexOfServiceWithSpecifiedUUID = bluetoothServices.indexOf({$0.UUIDString == serviceUUID}) else {
			peripheralVerbosity.DEBUG("No services with uuid : \(serviceUUID) were found\n") ; return false
		}
		guard let characteristic = bluetoothServices[indexOfServiceWithSpecifiedUUID].hasCharacteristic(characteristicUUID) else {
			peripheralVerbosity.DEBUG("Characteristic with : \(characteristicUUID) does not exist in service \(bluetoothServices[indexOfServiceWithSpecifiedUUID].debugDescription)\n")
			peripheralVerbosity.INFO("Characteristic with uuid \(characteristicUUID) was NOT removed from service with uuid \(serviceUUID)\n")
			return false
		}
		peripheralVerbosity.DEBUG("Service characteristics were: \(bluetoothServices[indexOfServiceWithSpecifiedUUID].characteristics)\n")
		
		bluetoothServices[indexOfServiceWithSpecifiedUUID].removeCharacteristic(characteristic)
		
		peripheralVerbosity.DEBUG("Service characteristics are now : \(bluetoothServices[indexOfServiceWithSpecifiedUUID].characteristics)\n")
		peripheralVerbosity.INFO("Characteristic with uuid \(characteristicUUID) was removed from service with uuid \(serviceUUID)\n")
		
		return true
	}
	
	internal func updateCharacteristic(characteristicUUID : CBUUID, forService serviceUUID: CBUUID, withNewValue value : NSData?) -> Bool{
		
		for index in 0..<bluetoothServices.count {
			if bluetoothServices[index].UUID == serviceUUID {
				guard let found = bluetoothServices[index].hasCharacteristic(characteristicUUID) else {
					peripheralVerbosity.DEBUG("No service has a characteristic with UUID \(characteristicUUID.debugDescription)")
					return false
				}
				peripheralVerbosity.DEBUG("Characteristich with UUID \(characteristicUUID.debugDescription) had value \(found.value.debugDescription)")
				bluetoothServices[index].updateValue(value, forCharacteristic: found.UUIDString)
				peripheralVerbosity.INFO("Characteristich with UUID \(characteristicUUID.debugDescription) updated with value \(value?.debugDescription)")
				return true
			}
		}
		
		peripheralVerbosity.DEBUG("There is no service with UUID \(serviceUUID.debugDescription)")
		
		return false
	}
}