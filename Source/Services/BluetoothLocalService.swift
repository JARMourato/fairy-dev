//
//  BluetoothLocalService.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

internal struct BluetoothLocalService: BluetoothUUID, CustomStringConvertible,CustomDebugStringConvertible {
	
	private var cbservice : CBMutableService
	private var cbcharacteristics : [BluetoothLocalCharacteristic]
	
	let UUIDString : String
	var primary : Bool
	
	var CoreBluetoothService : CBMutableService {
        get {
            cbservice.characteristics = cbcharacteristics.flatMap{ $0.CoreBluetoothCharacteristic }
            return cbservice
        }
	}
	
	var description : String {
        get {
            return cbservice.description
        }
	}
	
	var debugDescription : String {
        get {
            return cbservice.debugDescription
        }
	}
	
	var characteristics : [BluetoothLocalCharacteristic] {
        get {
            return cbcharacteristics
        }
	}
	
	init(uuidString : String, primaryService : Bool) {
        self.UUIDString = uuidString
        self.primary = primaryService
        cbcharacteristics = []
        cbservice = CBMutableService(type: CBUUID(string: self.UUIDString), primary: self.primary )
	}
	
	//MARK:- Methods over characteristics
	//MARK: Searching
	
	func hasCharacteristic(chararacteristicUUID : CBUUID) -> BluetoothLocalCharacteristic? {
		guard let found = cbcharacteristics.indexOf({$0.UUID == chararacteristicUUID}) else {
			return nil
		}
		return cbcharacteristics[found]
	}
	
	func hasCharacteristic(chararacteristicUUIDstring : String) -> BluetoothLocalCharacteristic? {
		guard let found = cbcharacteristics.indexOf({$0.UUIDString == chararacteristicUUIDstring}) else {
			return nil
		}
		return cbcharacteristics[found]
	}
	
	//MARK: Add & Remove
	
	mutating func addCharacteristic(characteristic : BluetoothLocalCharacteristic) -> Bool {
		guard hasCharacteristic(characteristic.UUIDString) == nil else { return false }
		cbcharacteristics.append(characteristic)
		return true
	}
	
	mutating func removeCharacteristic(characteristic : BluetoothLocalCharacteristic) -> Bool {
		guard let found = cbcharacteristics.indexOf({$0.UUIDString == characteristic.UUIDString}) else { return false }
		cbcharacteristics.removeAtIndex(found)
		return true
	}
	
	//MARK: Add & Remove subscribed centrals
	
	mutating func addSubscribedCentral(central : BluetoothRemoteCentral, forCharacteristic characteristic : BluetoothLocalCharacteristic) -> Bool{
		for index in 0..<cbcharacteristics.count {
			if cbcharacteristics[index].UUIDString == characteristic.UUIDString {
				return cbcharacteristics[index].addSubscribedCentral(central)
			}
		}
		return false
	}
	
	mutating func removeSubscribedCentral(central : BluetoothRemoteCentral, forCharacteristic characteristic : BluetoothLocalCharacteristic) -> Bool{
		for index in 0..<cbcharacteristics.count {
			if cbcharacteristics[index].UUIDString == characteristic.UUIDString {
				return cbcharacteristics[index].removeSubscribedCentral(central)
			}
		}
		return false
	}
	
	//MARK: Add & Remove centrals that failed to update
	
	mutating func addNotUpdatedSubscribedCentral(central : BluetoothRemoteCentral, forCharacteristic characteristic : BluetoothLocalCharacteristic) -> Bool{
		for index in 0..<cbcharacteristics.count {
			if cbcharacteristics[index].UUIDString == characteristic.UUIDString {
                return cbcharacteristics[index].addNotUpdatedCentral(central)
			}
		}
		return false
	}
	
	mutating func removeUpdatedCentral(central : BluetoothRemoteCentral, forCharacteristic characteristic : BluetoothLocalCharacteristic) -> Bool{
		for index in 0..<cbcharacteristics.count {
			if cbcharacteristics[index].UUIDString == characteristic.UUIDString {
                return cbcharacteristics[index].removeUpdatedCentral(central)
			}
		}
		return false
	}
	
	//MARK: Update characteristic value
	
	mutating func updateValue(value : NSData?, forCharacteristic characteristicUUID : String) -> Bool{
		for index in 0..<cbcharacteristics.count {
			if cbcharacteristics[index].UUIDString == characteristicUUID {
				cbcharacteristics[index].value = value
				return true
			}
		}
		return false
	}
}

extension BluetoothLocalService: Equatable {}

internal func ==(lhs: BluetoothLocalService, rhs: BluetoothLocalService) -> Bool{
    return lhs.UUIDString == rhs.UUIDString
    

//    guard lhs.UUIDString == rhs.UUIDString else { return false }
//    guard lhs.primary == rhs.primary else { return false }
//    
//    
//    return true
}
