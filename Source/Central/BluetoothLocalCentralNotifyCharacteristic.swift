//
//  BluetoothLocalCentralNotifyCharacteristic.swift
//  Fairy
//
//  Created by João Mourato on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothLocalCentral {
	
	public func subscribeCharacteristic(characteristic : String, service : String, deviceIdentifier : NSUUID, notifyBlock : NotifiedCharateristicsBlock) {
		notifyCharacteristic(true, characteristic: characteristic, service: service, deviceIdentifier: deviceIdentifier, notifyBlock: notifyBlock)
	}
	
	public func unsubscribeCharacteristic(characteristic : String, service : String, deviceIdentifier : NSUUID, notifyBlock : NotifiedCharateristicsBlock) {
		notifyCharacteristic(false, characteristic: characteristic, service: service, deviceIdentifier: deviceIdentifier, notifyBlock: notifyBlock)
	}
	
	private func notifyCharacteristic(notifyStatus : Bool, characteristic : String, service : String, deviceIdentifier : NSUUID, notifyBlock : NotifiedCharateristicsBlock) {
		let peripheral : BluetoothRemotePeripheral? = getConnectedPeripheralByIdentifier(deviceIdentifier)
		if let peripheralExists = peripheral {
			discoverServices(
				serviceUUIDs: [service],
				peripheral: peripheralExists,
				servicesDiscoveredBlock: { (services, error) -> (Void) in
					if error == .NoError {
						var serv : CBService? = nil
						if let servs = services {
							for s in servs {
								if s.UUIDString == service {
									serv = s.CoreBluetoothService
									break
								}
							}
						}
						if let serviceExists = serv {
							self.discoverCharacteristics(
								characteristicsUUIDs: [characteristic],
								service: serviceExists,
								peripheral: peripheralExists.CoreBluetoothPeripheral,
								characteristicsDiscoveredBlock: { (characteristics, error) -> (Void) in
									if error == .NoError {
										var cha : CBCharacteristic? = nil
										if let chars = characteristics {
											for ch in chars {
												if ch.UUIDString == characteristic {
													cha = ch.CoreBluetoothCharacteristic;
													break
												}
											}
										}
										if let characteristicExists = cha {
											self.notifyValueFromCharacteristic(notifyStatus, characteristic: characteristicExists, onPeripheral: peripheralExists.CoreBluetoothPeripheral, notifyBlock: notifyBlock)
										}
										else {
											notifyBlock?(data: nil, error: .CharacteristicNotProvidedByDevice)
										}
									}else {
										notifyBlock?(data: nil, error: .ErrorDiscoveringCharacteristics)
									}
							})
						} else {
							notifyBlock?(data: nil, error: .ServiceNotProvidedByDevice)
						}
					} else {
						notifyBlock?(data: nil, error: .ErrorDiscoveringServices)
					}
			})
			
		} else {
			notifyBlock?(data: nil, error: .DeviceProvidedIsNotConnected)
		}
	}

	//MARK - Set Characteristic Notification
	
	private func notifyValueFromCharacteristic(notifyEnabled : Bool, characteristic : CBCharacteristic, onPeripheral : CBPeripheral, notifyBlock : NotifiedCharateristicsBlock) {
		bluetoothNotifiedCharacteristic = notifyBlock
		
		onPeripheral.delegate = peripheralDelegateProxy
		onPeripheral.setNotifyValue(notifyEnabled, forCharacteristic: characteristic)
	}
	
}