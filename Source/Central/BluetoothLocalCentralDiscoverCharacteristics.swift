//
//  BluetoothLocalCentralDiscoverCharacteristics.swift
//  Fairy
//
//  Created by João Mourato on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothLocalCentral {
	
	public func discoverCharacteristicsOfDevice(identifier identifier : NSUUID, forServiceWithUUID : String, limitSearchToCharacteristicUUIDs: [String]? = nil, characteristicsDiscoveredBlock : DiscoveredCharateristicsBlock = nil){
		if let connectedPeripheral = getConnectedPeripheralByIdentifier(identifier) {
			var foundService : BluetoothRemoteService? = nil
			if let services = connectedPeripheral.services {
				for service in services
				{
					if service.UUID.isEqual(CBUUID(string: forServiceWithUUID)) {
						foundService = service
						break
					}
				}
			}
			if foundService != nil {
				discoverCharacteristics(characteristicsUUIDs: limitSearchToCharacteristicUUIDs, service: foundService!.CoreBluetoothService, peripheral: connectedPeripheral.CoreBluetoothPeripheral, characteristicsDiscoveredBlock: characteristicsDiscoveredBlock)
			}
			else {
				characteristicsDiscoveredBlock?(characteristics: nil, error: .ServiceNotProvidedByDevice)
			}
		}
		else {
			characteristicsDiscoveredBlock?(characteristics: nil, error: .DeviceProvidedIsNotConnected)
		}
	}
	
	internal func discoverCharacteristics(characteristicsUUIDs characteristicsUUIDs : [String]?, service : CBService, peripheral : CBPeripheral, characteristicsDiscoveredBlock : DiscoveredCharateristicsBlock) {
		bluetoothDiscoveredCharacteristics = characteristicsDiscoveredBlock
		peripheral.delegate = peripheralDelegateProxy
		var characteristics : [CBUUID]?
		if let c = characteristicsUUIDs {
			characteristics = c.flatMap{ CBUUID(string: $0 as String) }
		} else {
			characteristics = nil
		}
		peripheral.discoverCharacteristics(characteristics, forService: service)
	}

}