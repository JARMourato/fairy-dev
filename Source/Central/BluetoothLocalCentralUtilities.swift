//
//  BluetoothLocalCentralUtilities.swift
//  Fairy
//
//  Created by João Mourato on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothLocalCentral {
	
	// MARK: RSSI
	public func enableRSSIValueUpdatesForConnectedDevices(updateInterval : Double = 1.0) {
		rssiReadingTimer = NSTimer.scheduledTimerWithTimeInterval(updateInterval, target: self, selector: "readRSSIValuesOfConnectedDevices", userInfo: nil, repeats: true)
	}
	
	public func disableRSSIValueUpdatesForConnectedDevices() {
		rssiReadingTimer?.invalidate()
		rssiReadingTimer = nil
	}
	
	// MARK: Battery
	
	public func enableBatteryValueUpdatesForConnectedDevices() {
		batteryValueUpdatesForConnectedDevices(true)
	}
	
	public func disableBatteryValueUpdatesForConnectedDevices() {
		batteryValueUpdatesForConnectedDevices(false)
	}
	
    //MARK: Add new discoveredPeripheral
    
    internal func addNewDiscoveredPeripheral(peripheralDiscovered : BluetoothRemotePeripheral) {
        if let peripheralExists = getDiscoveredPeripheralByIdentifier(peripheralDiscovered.identifier) {
            if let index = discoveredPeripherals.indexOf({ $0.identifier == peripheralExists.identifier }) {
                discoveredPeripherals[index] = peripheralDiscovered
            }
        } else {
            discoveredPeripherals.append(peripheralDiscovered)
        }
    }
    
    
	//MARK: Get peripherals by identifier NSUUID
	
	internal func getConnectedPeripheralByIdentifier(identifier : NSUUID) -> BluetoothRemotePeripheral? {
		return getPeripheralByIdentifier(identifier, inArray: connectedPeripherals)
	}
	
	internal func getDiscoveredPeripheralByIdentifier(identifier : NSUUID) -> BluetoothRemotePeripheral? {
		return getPeripheralByIdentifier(identifier, inArray: discoveredPeripherals)
	}
	
	internal func getPeripheralByIdentifier(identifier : NSUUID, inArray : [BluetoothRemotePeripheral]) -> BluetoothRemotePeripheral? {
		for peripheral in inArray {
            if peripheral.identifier.isEqual(identifier){
                return peripheral
            }
		}
		return nil
	}
	
	//MARK - Read RSSI Values
	
	internal func readRSSIValuesOfConnectedDevices() {
		
		for p in connectedPeripherals {
            p.readRSSI()
		}
	}
	
	private func batteryValueUpdatesForConnectedDevices(enabled : Bool) {
		for p in connectedPeripherals {
            self.discoverServices(serviceUUIDs: [BluetoothLEProtocolDefinedService.Battery.service],
                peripheral: p,
                servicesDiscoveredBlock: {
                    (services, error) -> (Void) in
                    if let svs = services {
                        for sv in svs {
							if sv.CoreBluetoothService.UUID.isEqual(BluetoothLEProtocolDefinedService.Battery.serviceUUID){
								self.discoverCharacteristics(characteristicsUUIDs: BluetoothLEProtocolDefinedService.Battery.characteristics,
									service: sv.CoreBluetoothService,
									peripheral: p.CoreBluetoothPeripheral,
									characteristicsDiscoveredBlock: {
										(characteristics, error) -> (Void) in
										if let chs = characteristics {
											for ch in chs {
												if ch.CoreBluetoothCharacteristic.UUID.isEqual(BluetoothLEProtocolDefinedService.Battery.characteristicUUIDs[0]) {
													p.CoreBluetoothPeripheral.setNotifyValue(enabled, forCharacteristic: ch.CoreBluetoothCharacteristic)
													break
												}
											}
										}
								})
								break
							}
                        }
                    }
            })
		}
	}
	
}