//
//  BluetoothLocalCentralConfiguration.swift
//  Fairy
//
//  Created by João Mourato on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation
import CoreBluetooth

public class BluetoothLocalCentralConfiguration: NSObject {
	
	public var showAlert : Bool = false
	public var runOnMainThread : Bool = true
	public var defaultNameForDevices : String = "Unnamed"
	#if os(iOS)
	public var restoringKey : String? = nil//"com.fairy.bluetoothfairy"
	#endif
	public var centralVerbosity : Verbosity = .Debug
	//MARK: Delegate
	public var delegate : BluetoothLocalCentralDelegate? = nil
	
	
	//MARK: Blocks
	public var bluetoothChangedStateBlock : StateOfBluetoothChangedBlock = nil
    public var bluetoothDiscoveredDevicesBlock : DiscoveredDevicesBlock = nil
	public var bluetoothFailedToConnectWithDevice : FailedToConnectToDeviceBlock = nil
	public var bluetoothConnectedToDevice : ConnectedToDeviceBlock = nil
	public var bluetoothDisconnectedFromDevice : DisconnectedFromDeviceBlock = nil
	public var bluetoothForcedDisconnectedFromDevice : DisconnectedFromDeviceBlock = nil
	private var bluetoothDiscoveredServices : DiscoveredServicesBlock = nil
	private var bluetoothDiscoveredCharacteristics : DiscoveredCharateristicsBlock = nil
	private var bluetoothReadCharacteristic : ReadCharateristicsBlock = nil
	private var bluetoothNotifiedCharacteristic : NotifiedCharateristicsBlock = nil
	private var bluetoothWriteToCharacteristic : WriteCharateristicsBlock = nil

}