//
//  BluetoothCentral.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth


public class BluetoothRemoteCentral : NSObject, StringConvertible{
	
	private let cbcentral : CBCentral?
    private let cbMaximumUpdateValueLenght : Int?
    private var mockupCentral : Bool
    
    
	var UUIDString : String {
		get {
			return identifier.UUIDString
		}
	}
	
	public let identifier : NSUUID
    public var maximumUpdateValueLength : Int {
        get {
            return self.mockupCentral ? 0 : cbMaximumUpdateValueLenght!
        }
    }
	
	public var CoreBluetoothCentral : CBCentral {
		get {
            return self.mockupCentral ? cbcentral! : cbcentral!
		}
	}
	
	internal init(central : CBCentral) {
		self.cbcentral = central
		self.identifier = central.identifier
		self.cbMaximumUpdateValueLenght = central.maximumUpdateValueLength
        self.mockupCentral = false
	}
	
    internal init(mockUpIdentifier : NSUUID) {
        self.cbcentral = nil
        self.cbMaximumUpdateValueLenght = nil
        self.identifier = mockUpIdentifier
        self.mockupCentral = true
    }
    
    
	public func string() -> String {
		return cbcentral.debugDescription
	}
}

