//
//  BluetoothLocalCentralReadCharacteristic.swift
//  Fairy
//
//  Created by João Mourato on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothLocalCentral {
	
	public func readCharacteristic(characteristic : String, service : String, deviceIdentifier : NSUUID, readBlock : ReadCharateristicsBlock) {
		let peripheral : BluetoothRemotePeripheral? = getConnectedPeripheralByIdentifier(deviceIdentifier)
		if let peripheralExists = peripheral {
			discoverServices(
				serviceUUIDs: [service],
				peripheral: peripheralExists,
				servicesDiscoveredBlock: { (services, error) -> (Void) in
					if error == .NoError {
						var serv : CBService? = nil
						if let servs = services {
							for s in servs {
								if s.UUIDString == service {
									serv = s.CoreBluetoothService
									break
								}
							}
						}
						if let serviceExists = serv {
							self.discoverCharacteristics(
								characteristicsUUIDs: [characteristic],
								service: serviceExists,
								peripheral: peripheralExists.CoreBluetoothPeripheral,
								characteristicsDiscoveredBlock: { (characteristics, error) -> (Void) in
									if error == .NoError {
										var cha : CBCharacteristic? = nil
										if let chars = characteristics {
											for ch in chars {
												if ch.UUIDString == characteristic {
													cha = ch.CoreBluetoothCharacteristic;
													break
												}
											}
										}
										if let characteristicExists = cha {
											self.readValueFromCharacteristic(characteristicExists, onPeripheral: peripheralExists.CoreBluetoothPeripheral, readBlock: readBlock)
										}
										else {
											readBlock?(data: nil, error: .CharacteristicNotProvidedByDevice)
										}
									}else {
										readBlock?(data: nil, error: .ErrorDiscoveringCharacteristics)
									}
							})
						} else {
							readBlock?(data: nil, error: .ServiceNotProvidedByDevice)
						}
					} else {
						readBlock?(data: nil, error: .ErrorDiscoveringServices)
					}
			})
			
		} else {
			readBlock?(data: nil, error: .DeviceProvidedIsNotConnected)
		}
	}

	//MARK - Read Value From Characteristic
	
	private func readValueFromCharacteristic(characteristic : CBCharacteristic, onPeripheral : CBPeripheral, readBlock : ReadCharateristicsBlock) {
		bluetoothReadCharacteristic = readBlock
		onPeripheral.delegate = peripheralDelegateProxy
		onPeripheral.readValueForCharacteristic(characteristic)
	}
	
}
