//
//  BluetoothLocalCentralWriteCharacteristic.swift
//  Fairy
//
//  Created by João Mourato on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothLocalCentral {
	
	public func writeValueToCharacteristic(value : NSData?, characteristic : String, service : String, deviceIdentifier : NSUUID, writeBlock : WriteCharateristicsBlock) {
		if value == nil {
			delegate?.didWriteValueToCharacteristic?(value, error: .ErrorWritingNilValueToCharacteristic)
			writeBlock?(data: value, error: .ErrorWritingNilValueToCharacteristic)
			return;
		}
		let peripheral : BluetoothRemotePeripheral? = getConnectedPeripheralByIdentifier(deviceIdentifier)
		if let peripheralExists = peripheral {
			discoverServices(
				serviceUUIDs: [service],
				peripheral: peripheralExists,
				servicesDiscoveredBlock: { (services, error) -> (Void) in
					if error == .NoError {
						var serv : CBService? = nil
						if let servs = services {
							for s in servs {
								if s.UUIDString == service {
									serv = s.CoreBluetoothService
									break
								}
							}
						}
						if let serviceExists = serv {
							self.discoverCharacteristics(
								characteristicsUUIDs: [characteristic],
								service: serviceExists,
								peripheral: peripheralExists.CoreBluetoothPeripheral,
								characteristicsDiscoveredBlock: { (characteristics, error) -> (Void) in
									if error == .NoError {
										var cha : CBCharacteristic? = nil
										if let chars = characteristics {
											for ch in chars {
												if ch.UUIDString == characteristic {
													cha = ch.CoreBluetoothCharacteristic
													break
												}
											}
										}
										if let characteristicExists = cha {
											self.writeValueToCharacteristic(value!, characteristic: characteristicExists, onPeripheral: peripheralExists.CoreBluetoothPeripheral, writeBlock: writeBlock)
										}
										else {
											writeBlock?(data: nil, error: .CharacteristicNotProvidedByDevice)
										}
									}else {
										writeBlock?(data: nil, error: .ErrorDiscoveringCharacteristics)
									}
							})
						} else {
							writeBlock?(data: nil, error: .ServiceNotProvidedByDevice)
						}
					} else {
						writeBlock?(data: nil, error: .ErrorDiscoveringServices)
					}
			})
			
		} else {
			writeBlock?(data: nil, error: .DeviceProvidedIsNotConnected)
		}
	}
	
	//MARK - Write Value To Characteristic
	
	private func writeValueToCharacteristic(value : NSData, characteristic : CBCharacteristic, onPeripheral : CBPeripheral, writeBlock : WriteCharateristicsBlock) {
		bluetoothWriteToCharacteristic = writeBlock
		onPeripheral.delegate = peripheralDelegateProxy
		onPeripheral.writeValue(value, forCharacteristic: characteristic, type: .WithResponse)
	}
	
}