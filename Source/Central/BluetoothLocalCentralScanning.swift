//
//  BluetoothLocalCentralScanning.swift
//  Fairy
//
//  Created by João Mourato on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothLocalCentral {
	
	public func scanForDevices(
		enableFiltering enableFiltering : Bool = true,
		servicesUUIDs : [String]? = nil,
		servicesRequiredByDevice : [String]? = nil,
		failureBlock : FailedToScanForDevicesBlock = nil,
		successBlock : DiscoveredDevicesBlock = nil
		)
	{
		if !bluetoothManagerIsReady {
			if (verbosity == .Debug) {
				print("Error: Bluetooth manager is not ready and therefore cannot scan for devices")
			}
			failureBlock?(error: .ScanningIsNotAvailable)
			delegate?.didFailToScanPeripherals?(.ScanningIsNotAvailable)
			
			return
		}
		//TODO: verify if uuid strings are valid !!
		bluetoothDiscoveredDevicesBlock = successBlock
		
		//Restart array of discovered peripherals every new scan
		discoveredPeripherals = []
		//discoveredPeripheralsFormattedToUser = NSMutableArray()
		
		var services : [CBUUID]?
		
		if let s = servicesUUIDs {
			services = s.flatMap{ CBUUID(string: $0 as String) }
		} else {
			services = nil
		}
		
		var servicesPeripheralRequires : [CBUUID]
		
		if let sv = servicesRequiredByDevice {
			servicesPeripheralRequires = sv.flatMap{ CBUUID(string: $0 as String) }
		} else {
			servicesPeripheralRequires = []
		}
		
		manager?.scanForPeripheralsWithServices(services,
			options: [
				CBCentralManagerScanOptionAllowDuplicatesKey : !enableFiltering,
				CBCentralManagerScanOptionSolicitedServiceUUIDsKey : servicesPeripheralRequires
			])
		
		fallbackIsScanning = true
	}

	public func stopScanning() {
		manager?.stopScan()
		fallbackIsScanning = false
	}
	
	public func isScanningForDevices() -> Bool {
		#if os(iOS)
			if #available(iOS 9.0, *) {
				return (manager?.isScanning)!
			} else {
				return fallbackIsScanning
			}
		#elseif os(OSX)
			return fallbackIsScanning
		#endif
	}
	
}