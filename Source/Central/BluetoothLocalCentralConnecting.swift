//
//  BluetoothLocalCentralConnecting.swift
//  Fairy
//
//  Created by João Mourato on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothLocalCentral {
	
	public func connectToDevice(identifier identifier : NSUUID,
		backgroundNotification : BackgroundNotificationOfConnection = .None,
		onFailureToConnectBlock : FailedToConnectToDeviceBlock = nil,
		onDisconnectionBlock : DisconnectedFromDeviceBlock = nil,
		onConnectionBlock : ConnectedToDeviceBlock = nil) {
			
			connectToPeripheral(getDiscoveredPeripheralByIdentifier(identifier),
				backgroundNotification: backgroundNotification,
				failureToConnectBlock: onFailureToConnectBlock,
				onDisconnectionBlock: onDisconnectionBlock,
				onConnectionBlock: onConnectionBlock)
	}
	
	public func disconnectFromDevice(identifier identifier : NSUUID,
		onDisconnection : DisconnectedFromDeviceBlock = nil) {
			bluetoothForcedDisconnectedFromDevice = onDisconnection
			disconnectFromPeripheral(getConnectedPeripheralByIdentifier(identifier))
	}

	
	private func connectToPeripheral(peripheral : BluetoothRemotePeripheral?,
		backgroundNotification : BackgroundNotificationOfConnection,
		failureToConnectBlock : FailedToConnectToDeviceBlock,
		onDisconnectionBlock : DisconnectedFromDeviceBlock,
		onConnectionBlock : ConnectedToDeviceBlock
		){
			bluetoothFailedToConnectWithDevice = failureToConnectBlock
			bluetoothDisconnectedFromDevice = onDisconnectionBlock
			bluetoothConnectedToDevice = onConnectionBlock
			
			if let peripheral = peripheral {
				var flagDisconnection = false
				
				#if os(iOS)
					
					var flagConnection = false
					var flagNotification = false
					
					switch (backgroundNotification) {
					case .None: break
					case .OnDisconnection: flagDisconnection = true
					case .OnConnection: flagConnection = true
					case .OnNotification: flagNotification = true
					case .OnConnectionAndDisconnection:
						flagDisconnection = true
						flagConnection = true
					case .OnConnectionAndNotification:
						flagConnection = true
						flagNotification = true
					case .OnDisconnectionAndNotification:
						flagDisconnection = true
						flagNotification = true
					case .All:
						flagNotification = true
						flagDisconnection = true
						flagConnection = true
					}
					
				#elseif os(OSX)
					
					switch (backgroundNotification) {
					case .None: break
					case .OnDisconnection: flagDisconnection = true
					}
					
				#endif
				
				peripheral.delegate = peripheralDelegateProxy
				
				#if os(iOS)
					let optionsDic = [
						CBConnectPeripheralOptionNotifyOnDisconnectionKey : flagDisconnection,
						CBConnectPeripheralOptionNotifyOnConnectionKey : flagConnection,
						CBConnectPeripheralOptionNotifyOnNotificationKey : flagNotification
						
					]
				#elseif os(OSX)
					let optionsDic = [
						CBConnectPeripheralOptionNotifyOnDisconnectionKey : flagDisconnection
					]
				#endif
				
				manager?.connectPeripheral(peripheral.CoreBluetoothPeripheral, options: optionsDic)
			} else {
				if (verbosity == .Debug) {
					print("Error: There is no discovered peripheral with the provided info\n")
				}
                bluetoothFailedToConnectWithDevice?(peripheral : nil, error: .DeviceWithProvidedInfoNotFound)
                delegate?.didFailToConnectPeripheral?(nil, error: .DeviceWithProvidedInfoNotFound)
			}
	}
	
	
	private func disconnectFromPeripheral(peripheral : BluetoothRemotePeripheral?,
		onDisconnectionBlock : DisconnectedFromDeviceBlock = nil
		){
			if let disconnectBlock = onDisconnectionBlock {
				bluetoothDisconnectedFromDevice = disconnectBlock
			}
			if let p = peripheral{
				manager?.cancelPeripheralConnection(p.CoreBluetoothPeripheral)
                if let index = connectedPeripherals.indexOf({ $0.identifier == p.identifier }) {
                    connectedPeripherals.removeAtIndex(index)
                }
			} else {
				if (verbosity == .Debug) {
					print("Error: There is no peripheral with the provided info connected\n")
				}
			}
	}

	
	
}