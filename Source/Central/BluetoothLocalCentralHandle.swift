//
//  BluetoothLocalCentralHandle.swift
//  Fairy
//
//  Created by iOS on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothLocalCentral : CentralManagerProxyDelegate {
    
    func centralManagerDidUpdateState(state: BluetoothState) {
        switch (state) {
        case .PoweredOn:
            bluetoothManagerIsReady = true
        default:
            bluetoothManagerIsReady = false
        }
        
        centralConfiguration?.bluetoothChangedStateBlock?(state: state)
        delegate?.didChangedBluetoothState?(state)
    }
    
    func centralManagerDidDiscoverPeripheral(peripheral: BluetoothRemotePeripheral) {
        addNewDiscoveredPeripheral(peripheral)
        
        delegate?.didDiscoverPeripherals?(discoveredPeripherals)
        centralConfiguration?.bluetoothDiscoveredDevicesBlock?(discoveredDevices: discoveredPeripherals)
        // method call block subject to be changed when new call is made
        bluetoothDiscoveredDevicesBlock?(discoveredDevices: discoveredPeripherals)
    }
    
    func centralManagerDidFailToConnectPeripheral(peripheral: BluetoothRemotePeripheral, error: (bluetoothError: BluetoothError, errorDescription: String)?) {
        
        if let error = error { verbosity.ERROR(error.errorDescription) }
        
        delegate?.didFailToConnectPeripheral?(peripheral, error: error?.bluetoothError ?? .NoError)
        centralConfiguration?.bluetoothFailedToConnectWithDevice?(peripheral: peripheral, error: error?.bluetoothError ?? .NoError)
        // method call block subject to be changed when new call is made
        bluetoothFailedToConnectWithDevice?(peripheral: peripheral, error: error?.bluetoothError ?? .NoError)
    }
    
    func centralManagerDidConnectPeripheral(peripheral: BluetoothRemotePeripheral) {
        verbosity.DEBUG("did connect to peripheral :\(peripheral.debugDescription)")
		
		// Add device
		verbosity.DEBUG("Previously connected devices were \(connectedPeripherals.debugDescription)")
		connectedPeripherals.append(peripheral)
		verbosity.DEBUG("Connected devices are now \(connectedPeripherals.debugDescription)")
		
		
		delegate?.didConnectPeripheral?(peripheral)
        centralConfiguration?.bluetoothConnectedToDevice?(peripheral: peripheral)
        // method call block subject to be changed when new call is made
        bluetoothConnectedToDevice?(peripheral: peripheral)
    }
    
    func centralManagerDidDisconnectPeripheral(peripheral: BluetoothRemotePeripheral, error: (bluetoothError: BluetoothError, errorDescription: String)?) {
        if let error = error { verbosity.ERROR(error.errorDescription) }
		
		// Remove device
		if let index = connectedPeripherals.indexOf({ $0.identifier == peripheral.identifier }) {
			connectedPeripherals.removeAtIndex(index)
		}
		
        delegate?.didDisconnectFromPeripheral?(peripheral, error: error?.bluetoothError ?? .NoError)
        centralConfiguration?.bluetoothDisconnectedFromDevice?(peripheral: peripheral, error: error?.bluetoothError ?? .NoError)
        // method call block subject to be changed when new call is made
        bluetoothDisconnectedFromDevice?(peripheral: peripheral, error: error?.bluetoothError ?? .NoError)
        
		
    }
    
    func centralManagerDidRetrievePeripherals(peripherals: [BluetoothRemotePeripheral]) {
		//TODO:
    }
    
    func centralManagerDidRetrieveConnectedPeripherals(peripherals: [BluetoothRemotePeripheral]) {
        //TODO:
    }
    
    func centralManagerWillRestoreState(dict: [String : AnyObject]) {
        //TODO:
    }
    
}