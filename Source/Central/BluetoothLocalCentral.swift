//
//  BluetoothLocalCentral.swift
//  Fairy
//
//  Created by João Mourato on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import CoreBluetooth

#if os(OSX)
	import AppKit;
#elseif os(iOS)
	import UIKit;
#endif

public class BluetoothLocalCentral : NSObject {

	//MARK: - Central Manager and Delegate Proxy
	internal var manager : CBCentralManager? = nil
	internal var delegateProxy : CentralManagerDelegateProxy?
    internal var peripheralDelegateProxy : PeripheralDelegateProxy?
	
	//MARK: - Bluetooth Local Central Delegate
	internal var delegate : BluetoothLocalCentralDelegate?
	
	//MARK: - Verbosity
	internal var verbosity : Verbosity = .Off

	//MARK: - Bluetooth Blocks

//	internal var bluetoothChangedStateBlock : StateOfBluetoothChangedBlock
    internal var bluetoothDiscoveredDevicesBlock : DiscoveredDevicesBlock
	internal var bluetoothFailedToConnectWithDevice : FailedToConnectToDeviceBlock
	internal var bluetoothConnectedToDevice : ConnectedToDeviceBlock
	internal var bluetoothDisconnectedFromDevice : DisconnectedFromDeviceBlock
	internal var bluetoothForcedDisconnectedFromDevice : DisconnectedFromDeviceBlock
	internal var bluetoothDiscoveredServices : DiscoveredServicesBlock
	internal var bluetoothDiscoveredCharacteristics : DiscoveredCharateristicsBlock
	internal var bluetoothNotifiedCharacteristic : NotifiedCharateristicsBlock
	internal var bluetoothWriteToCharacteristic : WriteCharateristicsBlock
	
	internal var bluetoothReadCharacteristic : ReadCharateristicsBlock // should be temporary
	
	
	//MARK: - Central properties
	internal var centralConfiguration : BluetoothLocalCentralConfiguration?
	
	//MARK: - Flags
	internal var managerWasSetup : Bool = false
	internal var bluetoothManagerIsReady : Bool = false
	internal var fallbackIsScanning : Bool = false
	
	//MARK: - Arrays
	internal var discoveredPeripherals : [BluetoothRemotePeripheral] = []
	internal var connectedPeripherals : [BluetoothRemotePeripheral] = []
	internal var subscribedCharacteristics : [BluetoothRemoteCharacteristic] = []
	
	//MARK: - BluetoothRSSITimer
	internal var rssiReadingTimer : NSTimer?
	
	internal override init() {
		super.init()
		delegateProxy = CentralManagerDelegateProxy(delegate: self)
        peripheralDelegateProxy = PeripheralDelegateProxy(delegate: self)
	}
}