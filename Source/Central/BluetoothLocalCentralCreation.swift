//
//  BluetoothLocalCentralCreation.swift
//  Fairy
//
//  Created by João Mourato on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BluetoothLocalCentral {
	
	static public func createCentral(configuration : BluetoothLocalCentralConfiguration) -> BluetoothLocalCentral{
		
		let localCentral : BluetoothLocalCentral = BluetoothLocalCentral()
		
		var backgroundQueue : dispatch_queue_t? = nil
		
		var options : [String : AnyObject] = [CBCentralManagerOptionShowPowerAlertKey : configuration.showAlert]
		
		#if os(iOS)
			if let restoringKey = configuration.restoringKey {
				options = [
					CBCentralManagerOptionShowPowerAlertKey : configuration.showAlert,
					CBCentralManagerOptionRestoreIdentifierKey : restoringKey
				]
			}
			backgroundQueue = configuration.runOnMainThread ? nil : dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
		#elseif os(OSX)
			if #available(OSX 10.10, *) {
				backgroundQueue = configuration.runOnMainThread ? nil : dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)
			} else {
				backgroundQueue = configuration.runOnMainThread ? nil : dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
			}
		#endif
		
		
		localCentral.manager = CBCentralManager(delegate: localCentral.delegateProxy, queue: backgroundQueue, options: options)
		
		localCentral.delegate = configuration.delegate
		localCentral.verbosity = configuration.centralVerbosity
		
		localCentral.centralConfiguration = configuration
		
		return localCentral
	}
	
}