//
//  CBCentralManagerDelegate.swift
//  Fairy
//
//  Created by João Mourato on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation
import CoreBluetooth

internal protocol CentralManagerProxyDelegate {
	func centralManagerDidUpdateState(state: BluetoothState)
	func centralManagerDidDiscoverPeripheral(peripheral : BluetoothRemotePeripheral)
	func centralManagerDidConnectPeripheral(peripheral : BluetoothRemotePeripheral)
	func centralManagerDidFailToConnectPeripheral(peripheral : BluetoothRemotePeripheral, error: (bluetoothError : BluetoothError, errorDescription : String)?)
	func centralManagerDidDisconnectPeripheral(peripheral : BluetoothRemotePeripheral, error: (bluetoothError : BluetoothError, errorDescription : String)?)
	func centralManagerDidRetrievePeripherals(peripherals : [BluetoothRemotePeripheral])
	func centralManagerDidRetrieveConnectedPeripherals(peripherals : [BluetoothRemotePeripheral])
	func centralManagerWillRestoreState(dict: [String : AnyObject])
}

internal class CentralManagerDelegateProxy: NSObject, CBCentralManagerDelegate {

	private var delegate : CentralManagerProxyDelegate?
	private override init(){}
	
	init(delegate : CentralManagerProxyDelegate) {
		self.delegate = delegate
	}
	
	//MARK: - Bluetooth State
	func centralManagerDidUpdateState(central: CBCentralManager) {
		var state : BluetoothState
		switch (central.state) {
		case .Unknown :
			state = BluetoothState.Unknown
		case .Unsupported:
			state = BluetoothState.Unsupported
		case .Unauthorized:
			state = BluetoothState.Unauthorized
		case .Resetting:
			state = BluetoothState.Resetting
		case .PoweredOff:
			state = BluetoothState.PoweredOff
		case .PoweredOn:
			state = BluetoothState.PoweredOn
		}
		delegate?.centralManagerDidUpdateState(state)
	}
	
	//MARK: - State Restoration
	
	func centralManager(central: CBCentralManager, willRestoreState dict: [String : AnyObject]) {
		delegate?.centralManagerWillRestoreState(dict)
	}

	//MARK: - Discovering peripherals
	
	func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
		delegate?.centralManagerDidDiscoverPeripheral(BluetoothRemotePeripheral(peripheral: peripheral, advertisementData: advertisementData, RSSI: RSSI))
	}

	//MARK: - Connections with Peripherals
	
	func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
		delegate?.centralManagerDidConnectPeripheral(BluetoothRemotePeripheral(peripheral: peripheral))
	}
	
	func centralManager(central: CBCentralManager, didFailToConnectPeripheral peripheral: CBPeripheral, error: NSError?) {
		var err : (bluetoothError : BluetoothError, errorDescription : String)? = nil
		if let receivedError = error {
			err = (.FailedToConnectToDevice, receivedError.debugDescription)
		}
		delegate?.centralManagerDidFailToConnectPeripheral(BluetoothRemotePeripheral(peripheral: peripheral), error: err)
	}
	
	func centralManager(central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: NSError?) {
		var err : (bluetoothError : BluetoothError, errorDescription : String)? = nil
		if let receivedError = error {
			err = (.DeviceDisconnected, receivedError.debugDescription)
		}
		delegate?.centralManagerDidDisconnectPeripheral(BluetoothRemotePeripheral(peripheral: peripheral), error: err)
	}
	
	func centralManager(central: CBCentralManager, didRetrievePeripherals peripherals: [CBPeripheral]) {
		delegate?.centralManagerDidRetrievePeripherals(peripherals.flatMap{BluetoothRemotePeripheral(peripheral: $0)})
	}
	
	func centralManager(central: CBCentralManager, didRetrieveConnectedPeripherals peripherals: [CBPeripheral]) {
		delegate?.centralManagerDidRetrieveConnectedPeripherals(peripherals.flatMap{BluetoothRemotePeripheral(peripheral: $0)})
	}
	
}
