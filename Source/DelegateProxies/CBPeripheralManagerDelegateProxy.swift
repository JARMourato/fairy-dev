//
//  CBPeripheralManagerDelegateProxy.swift
//  JMBluetooth
//
//  Created by João Mourato on 14/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth


internal protocol PeripheralManagerProxyDelegate {
	func peripheralManagerDidUpdateState(state: BluetoothState)
	func peripheralManager(didAddService service : BluetoothRemoteService, error: (bluetoothError : BluetoothError, errorDescription : String)?)
	func peripheralManagerDidStartAdvertising(error: (bluetoothError : BluetoothError, errorDescription : String)?)
	func peripheralManager(central central: BluetoothRemoteCentral, didSubscribeToCharacteristic characteristic: BluetoothRemoteCharacteristic)
	func peripheralManager(central central: BluetoothRemoteCentral, didUnsubscribeFromCharacteristic characteristic: BluetoothRemoteCharacteristic)
	func peripheralManagerIsReadyToUpdateSubscribers()
	func peripheralManagerDidReceiveReadRequest(request: BluetoothReadRequest)
	func peripheralManagerDidReceiveWriteRequests(requests: [BluetoothWriteRequest])
	func peripheralManagerWillRestoreState(dict: [String : AnyObject])
}


internal class PeripheralManagerDelegateProxy: NSObject, CBPeripheralManagerDelegate {
	
	private var delegate : PeripheralManagerProxyDelegate?
	private override init(){}
	
	init(delegate : PeripheralManagerProxyDelegate) {
		self.delegate = delegate
	}
	
	//MARK: -  Bluetooth State
	
	internal func peripheralManagerDidUpdateState(peripheral: CBPeripheralManager) {
		var state : BluetoothState
		switch (peripheral.state) {
		case .Unknown :
			state = BluetoothState.Unknown
		case .Unsupported:
			state = BluetoothState.Unsupported
		case .Unauthorized:
			state = BluetoothState.Unauthorized
		case .Resetting:
			state = BluetoothState.Resetting
		case .PoweredOff:
			state = BluetoothState.PoweredOff
		case .PoweredOn:
			state = BluetoothState.PoweredOn
		}
		delegate?.peripheralManagerDidUpdateState(state)
	}
	
	//MARK: - Adding Service
	
	func peripheralManager(peripheral: CBPeripheralManager, didAddService service: CBService, error: NSError?) {
		var err : (bluetoothError : BluetoothError, errorDescription : String)? = nil
		if let receivedError = error {
			err = (.ErrorAddingService, receivedError.debugDescription)
		}
		delegate?.peripheralManager(didAddService: BluetoothRemoteService(service: service), error: err)
	}
	
	//MARK: - Advertising
	
	func peripheralManagerDidStartAdvertising(peripheral: CBPeripheralManager, error: NSError?) {
		var err : (bluetoothError : BluetoothError, errorDescription : String)? = nil
		if let receivedError = error {
			err = (.ErrorOnStartingAdvertisement, receivedError.debugDescription)
		}
		delegate?.peripheralManagerDidStartAdvertising(err)
	}
	
	//MARK: - Central subscribing characteristics
	
	func peripheralManager(peripheral: CBPeripheralManager, central: CBCentral, didSubscribeToCharacteristic characteristic: CBCharacteristic) {
		delegate?.peripheralManager(central: BluetoothRemoteCentral(central: central), didSubscribeToCharacteristic: BluetoothRemoteCharacteristic(characteristic: characteristic))
	}
	
	func peripheralManager(peripheral: CBPeripheralManager, central: CBCentral, didUnsubscribeFromCharacteristic characteristic: CBCharacteristic) {
		delegate?.peripheralManager(central: BluetoothRemoteCentral(central: central), didUnsubscribeFromCharacteristic: BluetoothRemoteCharacteristic(characteristic: characteristic))
	}
	
	//MARK: - Peripheral Manager Readiness
	
	func peripheralManagerIsReadyToUpdateSubscribers(peripheral: CBPeripheralManager) {
		delegate?.peripheralManagerIsReadyToUpdateSubscribers()
	}
	
	//MARK: - Read/Write Requests
	
	func peripheralManager(peripheral: CBPeripheralManager, didReceiveReadRequest request: CBATTRequest) {
		delegate?.peripheralManagerDidReceiveReadRequest(BluetoothReadRequest(remoteRequest: request))
	}
	
	func peripheralManager(peripheral: CBPeripheralManager, didReceiveWriteRequests requests: [CBATTRequest]) {
		delegate?.peripheralManagerDidReceiveWriteRequests(requests.flatMap{BluetoothWriteRequest(remoteRequest: $0)})
	}
	
	func peripheralManager(peripheral: CBPeripheralManager, willRestoreState dict: [String : AnyObject]) {
		delegate?.peripheralManagerWillRestoreState(dict)
	}
	
}