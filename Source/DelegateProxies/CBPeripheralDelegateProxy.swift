//
//  CBPeripheralDelegateProxy.swift
//  Fairy
//
//  Created by iOS on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation
import CoreBluetooth

internal protocol PeripheralProxyDelegate {
    func peripheralDidUpdateRSSI(peripheral : BluetoothRemotePeripheral, error: (bluetoothError : BluetoothError, errorDescription : String)?)
    func peripheralDidUpdateName(peripheral : BluetoothRemotePeripheral)
    func peripheralDidInvalidateServices(peripheral : BluetoothRemotePeripheral)
    func peripheral(peripheral : BluetoothRemotePeripheral, didModifyServices invalidatedServices : [BluetoothRemoteService])
    func peripheralDidDiscoverServices(peripheral : BluetoothRemotePeripheral, error: (bluetoothError : BluetoothError, errorDescription : String)?)
    func peripheralDidDiscoverIncludedServices(peripheral : BluetoothRemotePeripheral, forService service : BluetoothRemoteService, error: (bluetoothError : BluetoothError, errorDescription : String)?)
    func peripheralDidDiscoverCharacteristics(peripheral : BluetoothRemotePeripheral, forService service : BluetoothRemoteService, error: (bluetoothError : BluetoothError, errorDescription : String)?)
    func peripheralDidDiscoverDescriptors(peripheral : BluetoothRemotePeripheral, forCharacteristic characteristic : BluetoothRemoteCharacteristic, error: (bluetoothError : BluetoothError, errorDescription : String)?)
    func peripheralDidUpdateValueForDescriptor(peripheral : BluetoothRemotePeripheral, descriptor : BluetoothRemoteDescriptor, error: (bluetoothError : BluetoothError, errorDescription : String)?)
    func peripheralDidWriteValueForDescriptor(peripheral : BluetoothRemotePeripheral, descriptor : BluetoothRemoteDescriptor, error: (bluetoothError : BluetoothError, errorDescription : String)?)
    func peripheralDidUpdateValueForCharacteristic(peripheral : BluetoothRemotePeripheral, characteristic : BluetoothRemoteCharacteristic, error: (bluetoothError : BluetoothError, errorDescription : String)?)
    func peripheralDidWriteValueForCharacteristic(peripheral : BluetoothRemotePeripheral, characteristic : BluetoothRemoteCharacteristic, error: (bluetoothError : BluetoothError, errorDescription : String)?)
    func peripheralDidUpdateNotificationStateForCharacteristic(peripheral : BluetoothRemotePeripheral, characteristic : BluetoothRemoteCharacteristic, error: (bluetoothError : BluetoothError, errorDescription : String)?)
}


internal class PeripheralDelegateProxy : NSObject, CBPeripheralDelegate {
    
    private var delegate : PeripheralProxyDelegate?
    private override init(){}
    
    init(delegate : PeripheralProxyDelegate) {
        self.delegate = delegate
    }
	
	func peripheral(peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: NSError?) {
		var err : (bluetoothError : BluetoothError, errorDescription : String)? = nil
		let peripheral = BluetoothRemotePeripheral(peripheral: peripheral)
		if let receivedError = error {
			err = (.ErrorUpdatingRSSIValue, receivedError.debugDescription)
		}
		else {
			peripheral.rssi = RSSI
		}
		delegate?.peripheralDidUpdateRSSI(peripheral, error: err)
	}
	
    func peripheralDidUpdateName(peripheral: CBPeripheral) {
        delegate?.peripheralDidUpdateName(BluetoothRemotePeripheral(peripheral: peripheral))
    }
    
    //MARK: - Services
    
    func peripheralDidInvalidateServices(peripheral: CBPeripheral) {
        delegate?.peripheralDidInvalidateServices(BluetoothRemotePeripheral(peripheral: peripheral))
    }
    
    func peripheral(peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
        delegate?.peripheral(BluetoothRemotePeripheral(peripheral: peripheral), didModifyServices: invalidatedServices.flatMap{BluetoothRemoteService(service: $0)})
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverServices error: NSError?) {
        var err : (bluetoothError : BluetoothError, errorDescription : String)? = nil
        if let receivedError = error {
            err = (.ErrorDiscoveringServices, receivedError.debugDescription)
        }
        delegate?.peripheralDidDiscoverServices(BluetoothRemotePeripheral(peripheral: peripheral), error: err)
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverIncludedServicesForService service: CBService, error: NSError?) {
        var err : (bluetoothError : BluetoothError, errorDescription : String)? = nil
        if let receivedError = error {
            err = (.ErrorDiscoveringIncludedServices, receivedError.debugDescription)
        }
        delegate?.peripheralDidDiscoverIncludedServices(BluetoothRemotePeripheral(peripheral: peripheral), forService: BluetoothRemoteService(service: service), error: err)
    }
    
    //MARK: - Characteristics
    //MARK: - Discovering descriptors and characteristics
    
    func peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService service: CBService, error: NSError?) {
        var err : (bluetoothError : BluetoothError, errorDescription : String)? = nil
        if let receivedError = error {
            err = (.ErrorDiscoveringCharacteristics, receivedError.debugDescription)
        }
        delegate?.peripheralDidDiscoverCharacteristics(BluetoothRemotePeripheral(peripheral: peripheral), forService: BluetoothRemoteService(service: service), error: err)
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverDescriptorsForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        var err : (bluetoothError : BluetoothError, errorDescription : String)? = nil
        if let receivedError = error {
            err = (.ErrorDiscoveringCharacteristicDescriptors, receivedError.debugDescription)
        }
        delegate?.peripheralDidDiscoverDescriptors(BluetoothRemotePeripheral(peripheral: peripheral), forCharacteristic: BluetoothRemoteCharacteristic(characteristic: characteristic), error: err)
    }
    
    //MARK: - Update & Write value for descriptors
    
    func peripheral(peripheral: CBPeripheral, didUpdateValueForDescriptor descriptor: CBDescriptor, error: NSError?) {
        var err : (bluetoothError : BluetoothError, errorDescription : String)? = nil
        if let receivedError = error {
            err = (.ErrorUpdatingCharacteristicDescriptors, receivedError.debugDescription)
        }
        delegate?.peripheralDidUpdateValueForDescriptor(BluetoothRemotePeripheral(peripheral: peripheral), descriptor: BluetoothRemoteDescriptor(descriptor: descriptor), error: err)
    }
    
    func peripheral(peripheral: CBPeripheral, didWriteValueForDescriptor descriptor: CBDescriptor, error: NSError?) {
        var err : (bluetoothError : BluetoothError, errorDescription : String)? = nil
        if let receivedError = error {
            err = (.ErrorWritingCharacteristicDescriptors, receivedError.debugDescription)
        }
        delegate?.peripheralDidWriteValueForDescriptor(BluetoothRemotePeripheral(peripheral: peripheral), descriptor: BluetoothRemoteDescriptor(descriptor: descriptor), error: err)
    }
    
    //MARK: - Update & Write value for characteristic
    
    func peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        var err : (bluetoothError : BluetoothError, errorDescription : String)? = nil
        if let receivedError = error {
            err = (.ErrorUpdatingCharacteristicValue, receivedError.debugDescription)
        }
        delegate?.peripheralDidUpdateValueForCharacteristic(BluetoothRemotePeripheral(peripheral: peripheral), characteristic: BluetoothRemoteCharacteristic(characteristic: characteristic), error: err)
    }
    
    func peripheral(peripheral: CBPeripheral, didWriteValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        var err : (bluetoothError : BluetoothError, errorDescription : String)? = nil
        if let receivedError = error {
            err = (.ErrorOnWritingValueToCharacteristic, receivedError.debugDescription)
        }
        delegate?.peripheralDidWriteValueForCharacteristic(BluetoothRemotePeripheral(peripheral: peripheral), characteristic: BluetoothRemoteCharacteristic(characteristic: characteristic), error: err)
    }
    
    //MARK: - Notifications
    
    func peripheral(peripheral: CBPeripheral, didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        var err : (bluetoothError : BluetoothError, errorDescription : String)? = nil
        if let receivedError = error {
            err = (.ErrorUpdatingCharacteristicNotificationState, receivedError.debugDescription)
        }
        delegate?.peripheralDidUpdateNotificationStateForCharacteristic(BluetoothRemotePeripheral(peripheral: peripheral), characteristic: BluetoothRemoteCharacteristic(characteristic: characteristic), error: err)
    }
    
}