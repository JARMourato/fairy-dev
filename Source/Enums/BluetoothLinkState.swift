//
//  BluetoothLinkState.swift
//  JMBluetooth-OSX-Example
//
//  Created by João Mourato on 15/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

@objc public enum BluetoothPeripheralLinkState : Int {
	case Disconnected = 0
	case Connecting
	case Connected
    case Unknown
}

internal extension BluetoothPeripheralLinkState {
	
	internal init(state : CBPeripheralState) {
        #if os(iOS)
		switch state {
		case .Connected : self = .Connected
		case .Disconnected : self = .Disconnected
		case .Connecting : self = .Connecting
        default: self = .Unknown
		}
        #elseif os(OSX)
        switch state {
        case .Connected : self = .Connected
        case .Disconnected : self = .Disconnected
        case .Connecting : self = .Connecting
        }
        #endif
	}
	
	func stateInCoreBluetooth() -> CBPeripheralState {
		switch self {
		case .Connected : return .Connected
		case .Disconnected : return .Disconnected
		case .Connecting : return .Connecting
        default: return .Disconnected
		}
	}
}

extension BluetoothPeripheralLinkState : StringConvertible{
	public func string() -> String {
		return ["Connected", "Disconnected", "Connecting"][self.rawValue]
	}
}