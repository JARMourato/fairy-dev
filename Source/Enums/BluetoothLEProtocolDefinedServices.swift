//
//  BluetoothLEProtocolDefinedServices.swift
//  JMBluetooth-OSX-Example
//
//  Created by João Mourato on 15/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

internal struct BLEDefinedService : StringConvertible {
	var service : String = ""
	var characteristics : [String] = []
	
	init(service : String, characteristics : [String]) {
		self.service = service
		self.characteristics = characteristics
	}
	
	var serviceUUID : CBUUID {
		get {
			return CBUUID(string: service)
		}
	}
	
	var characteristicUUIDs : [CBUUID] {
		get {
			return characteristics.flatMap{ CBUUID(string: $0) }
		}
	}
	
	func string() -> String {
		return "Service :\(service) and characteristics :\(characteristics)"
	}
}


internal struct BluetoothLEProtocolDefinedService {
	static let Battery = BLEDefinedService(service: "180F", characteristics: ["2A19"])
	
}

