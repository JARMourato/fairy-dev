//
//  BluetoothCharacteristicValueAttributePermissions.swift
//  JMBluetooth-OSX-Example
//
//  Created by João Mourato on 15/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

@objc public enum BluetoothCharacteristicValueAttributePermissions : Int {
	case Readable
	case Writeable
	case ReadEncryptionRequired
	case WriteEncryptionRequired
}

extension BluetoothCharacteristicValueAttributePermissions: StringConvertible {
	func convertToCBAttributePermission() -> CBAttributePermissions {
		switch self {
		case .Readable: return .Readable
		case .Writeable: return .Writeable
		case .ReadEncryptionRequired: return .ReadEncryptionRequired
		case .WriteEncryptionRequired: return .WriteEncryptionRequired
		}
	}
	
	static func concatMultipleProperties(var permissions: [BluetoothCharacteristicValueAttributePermissions]) -> CBAttributePermissions? {
		guard permissions.count > 0 else { return nil }
		
		var characteristicPermissions : CBAttributePermissions = permissions.removeAtIndex(0).convertToCBAttributePermission()
		
		while permissions.count > 0 {
			characteristicPermissions.insert(permissions.removeAtIndex(0).convertToCBAttributePermission())
		}
		
		return characteristicPermissions
	}
	
	public func string() -> String {
		return ["Readable", "Writeable", "ReadEncryptionRequired", "WriteEncryptionRequired"][self.rawValue]
	}
}

