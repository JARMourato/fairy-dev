//
//  BluetoothCharacteristicProperties.swift
//  JMBluetooth-OSX-Example
//
//  Created by João Mourato on 15/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation
import CoreBluetooth

@objc public enum BluetoothCharacteristicProperties : Int {
	case Broadcast = 0
	case Read
	case WriteWithoutResponse
	case Write
	case Notify
	case Indicate
	case AuthenticatedSignedWrites
	case ExtendedProperties
	case NotifyEncryptionRequired
	case IndicateEncryptionRequired
}

extension BluetoothCharacteristicProperties: CustomStringConvertible, CustomDebugStringConvertible {
	
	static func convertToBluetoothCharacteristicProperties(properties : CBCharacteristicProperties) -> [BluetoothCharacteristicProperties]{
		var bluetoothProperties : [BluetoothCharacteristicProperties] = []
		
		if properties.contains(.Broadcast) {
			bluetoothProperties.append(.Broadcast)
		}
		if properties.contains(.Read) {
			bluetoothProperties.append(.Read)
		}
		if properties.contains(.WriteWithoutResponse) {
			bluetoothProperties.append(.WriteWithoutResponse)
		}
		if properties.contains(.Write) {
			bluetoothProperties.append(.Write)
		}
		if properties.contains(.Notify) {
			bluetoothProperties.append(.Notify)
		}
		if properties.contains(.Indicate) {
			bluetoothProperties.append(.Indicate)
		}
		if properties.contains(.AuthenticatedSignedWrites) {
			bluetoothProperties.append(.AuthenticatedSignedWrites)
		}
		if properties.contains(.ExtendedProperties) {
			bluetoothProperties.append(.ExtendedProperties)
		}
		if properties.contains(.NotifyEncryptionRequired) {
			bluetoothProperties.append(.NotifyEncryptionRequired)
		}
		if properties.contains(.IndicateEncryptionRequired) {
			bluetoothProperties.append(.IndicateEncryptionRequired)
		}
		
		return bluetoothProperties
	}
	
	func convertToCBCharacteristicProperty() -> CBCharacteristicProperties {
		switch self {
		case .Broadcast: return .Broadcast
		case .Read: return .Read
		case .WriteWithoutResponse: return .WriteWithoutResponse
		case .Write: return .Write
		case .Notify: return .Notify
		case .Indicate: return .Indicate
		case .AuthenticatedSignedWrites: return .AuthenticatedSignedWrites
		case .ExtendedProperties: return .ExtendedProperties
		case .NotifyEncryptionRequired: return .NotifyEncryptionRequired
		case .IndicateEncryptionRequired: return .IndicateEncryptionRequired
		}
	}
	
	static func concatMultipleProperties(var properties: [BluetoothCharacteristicProperties]) -> CBCharacteristicProperties? {
		guard properties.count > 0 else { return nil }
		
		var characteristicProperties : CBCharacteristicProperties = properties.removeAtIndex(0).convertToCBCharacteristicProperty()
		
		while properties.count > 0 {
			characteristicProperties.insert(properties.removeAtIndex(0).convertToCBCharacteristicProperty())
		}
		
		return characteristicProperties
	}
	
	func string() -> String {
		return ["Broadcast", "Read", "WriteWithoutResponse", "Write", "Notify", "Indicate",
			"AuthenticatedSignedWrites", "ExtendedProperties", "NotifyEncryptionRequired",
			"IndicateEncryptionRequired"][self.rawValue]
	}
	
	public var description:String {
		get {
			return string()
		}
	}
	
	public var debugDescription:String {
		get {
			return string()
		}
	}
}