//
//  Verbosity.swift
//  JMBluetooth-OSX-Example
//
//  Created by João Mourato on 15/01/16.
//  Copyright © 2016 JARM. All rights reserved.
//

import Foundation

@objc public enum Verbosity : Int {
	case Off = 0
	case Info
	case Error
	case Debug
}

extension Verbosity {
	
	func stringMe() -> String {
		return ["Off", "Info", "Error", "Debug"][self.rawValue]
	}
	
	func formatStringOutput(levelString : String, message : String) -> String {
		return "\n\(levelString): \(message)"
	}
	
	func printVerbosityMessage(messageString : String, minimumLevel : Verbosity) {
		if self >= minimumLevel {
			print(formatStringOutput(stringMe(), message: messageString))
		}
	}
	
	func INFO(message : String) {
		printVerbosityMessage(message, minimumLevel: .Info)
	}
	
	func ERROR(message : String) {
		printVerbosityMessage(message, minimumLevel: .Error)
	}
	
	func DEBUG(message : String) {
		printVerbosityMessage(message, minimumLevel: .Debug)
	}
	
//	func DEV(message : String) {
//		printVerbosityMessage(message, minimumLevel: .Dev)
//	}
}

func >=(lhs: Verbosity, rhs: Verbosity) -> Bool{
	return lhs.rawValue >= rhs.rawValue
}
