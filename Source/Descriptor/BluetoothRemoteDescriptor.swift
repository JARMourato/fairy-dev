//
//  BluetoothRemoteDescriptor.swift
//  Fairy
//
//  Created by iOS on 18/01/16.
//  Copyright © 2016 JARMPods. All rights reserved.
//

import Foundation
import CoreBluetooth


internal struct BluetoothRemoteDescriptor {
    private var cbdescriptor : CBDescriptor
    
    internal var CoreBluetoothDescriptor : CBDescriptor {
        return cbdescriptor
    }
    
    init(descriptor : CBDescriptor) {
        self.cbdescriptor = descriptor
    }
    
}